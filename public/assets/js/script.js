$(document).on('click', '#btn-edit', function () {
    $('.modal-body #id_karyawan').val($(this).data('id'));
    $('.modal-body #nomor_karyawan').val($(this).data('nomor'));
    $('.modal-body #nama_karyawan').val($(this).data('nama'));
    $('.modal-body #divisi').val($(this).data('divisi'));
    $('.modal-body #jabatan').val($(this).data('jabatan'));
    $('.modal-body #tanggal_masuk').val($(this).data('tanggal-masuk'));
    $('.modal-body #tanggal_lahir').val($(this).data('tanggal-lahir'));
    $('.modal-body #agama').val($(this).data('agama'));
    $('.modal-body #alamat').val($(this).data('alamat'));
})

$(document).on('click', '#btn-info', function () {
    $('.modal-body #id_karyawan').val($(this).data('id'));
    $('.modal-body #nomor_karyawan').val($(this).data('nomor'));
    $('.modal-body #nama_karyawan').val($(this).data('nama'));
    $('.modal-body #divisi').val($(this).data('divisi'));
    $('.modal-body #jabatan').val($(this).data('jabatan'));
    $('.modal-body #tanggal_masuk').val($(this).data('tanggal-masuk'));
    $('.modal-body #tanggal_lahir').val($(this).data('tanggal-lahir'));
    $('.modal-body #agama').val($(this).data('agama'));
    $('.modal-body #alamat').val($(this).data('alamat'));
})

// Sweet Alert 2
const swal = $('.swal').data('swal');
if (swal) {
    Swal.fire({
        title: 'Pendaftaran berhasil',
        text: swal,
        icon: 'success'
    })
}

const swal2 = $('.swal2').data('swal2');
if (swal2) {
    Swal.fire({
        title: swal2,
        text: swal,
        icon: 'success'
    })
}

const swal3 = $('.swal3').data('swal3');
if (swal3) {
    Swal.fire({
        title: 'Pengunduran diri berhasil ditambahkan',
        text: swal,
        icon: 'success'
    })
}

const swal4 = $('.swal4').data('swal4');
if (swal4) {
    Swal.fire({
        title: 'Pelatihan berhasil ditambahkan',
        text: swal,
        icon: 'success'
    })
}

const swal5 = $('.swal5').data('swal5');
if (swal5) {
    Swal.fire({
        title: swal5,
        text: swal,
        icon: 'success'
    })
}