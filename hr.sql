-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2021 at 11:23 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hr`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id_absensi` int(11) NOT NULL,
  `nama_karyawan` varchar(255) NOT NULL,
  `nomor_karyawan` varchar(255) NOT NULL,
  `divisi` varchar(255) NOT NULL,
  `waktu` time DEFAULT current_timestamp(),
  `tanggal` date DEFAULT current_timestamp(),
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id_absensi`, `nama_karyawan`, `nomor_karyawan`, `divisi`, `waktu`, `tanggal`, `created_at`, `updated_at`) VALUES
(1, '11180930000075', 'Sulthan Muhammad Raihan', 'HR', NULL, NULL, '2021-06-05 03:28:08', '2021-06-05 03:28:08'),
(2, '11180930000076', 'Muhammad Faiz Kurniawan', 'Inventory', NULL, NULL, '2021-06-05 04:00:33', '2021-06-05 04:00:33'),
(3, '11180930000079', 'Muhammad Alfarizy', 'Purchasing', NULL, NULL, '2021-06-05 18:59:05', '2021-06-05 18:59:05'),
(4, '11180930000079', 'Muhammad Alfarizy', 'Purchasing', NULL, NULL, '2021-06-07 09:02:38', '2021-06-07 09:02:38'),
(5, '11180930000063', 'Galih Alif Farizky', 'Finance', NULL, NULL, '2021-06-07 09:03:40', '2021-06-07 09:03:40'),
(6, '11180930000069', 'Muhammad Harits Daud', 'HR', NULL, NULL, '2021-06-07 09:07:14', '2021-06-07 09:07:14'),
(7, '11180930000076', 'Muhammad Faiz Kurniawan', 'Inventory', NULL, NULL, '2021-06-07 09:10:48', '2021-06-07 09:10:48'),
(8, '11180930000079', 'Muhammad Alfarizy', 'Purchasing', NULL, NULL, '2021-06-10 02:56:19', '2021-06-10 02:56:19'),
(9, '11180930000069', 'Farhan Haryono', 'Sales & Marketing', NULL, NULL, '2021-06-10 07:56:19', '2021-06-10 07:56:19'),
(10, '11180930000075', 'Sulthan Muhammad Raihan', 'HR', NULL, NULL, '2021-06-10 20:11:05', '2021-06-10 20:11:05');

-- --------------------------------------------------------

--
-- Table structure for table `auth_activation_attempts`
--

CREATE TABLE `auth_activation_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups`
--

CREATE TABLE `auth_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups_permissions`
--

CREATE TABLE `auth_groups_permissions` (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `permission_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups_users`
--

CREATE TABLE `auth_groups_users` (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_logins`
--

CREATE TABLE `auth_logins` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `date` datetime NOT NULL,
  `success` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_logins`
--

INSERT INTO `auth_logins` (`id`, `ip_address`, `email`, `user_id`, `date`, `success`) VALUES
(1, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-06 03:33:42', 1),
(2, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-06 06:31:45', 1),
(3, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-06 06:35:12', 1),
(4, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-06 07:32:52', 1),
(5, '::1', 'admin', NULL, '2021-06-06 07:33:13', 0),
(6, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-06 07:33:23', 1),
(7, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-06 07:35:18', 1),
(8, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-06 07:37:08', 1),
(9, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-06 07:37:35', 1),
(10, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-06 07:38:18', 1),
(11, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-06 07:41:45', 1),
(12, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-06 07:44:48', 1),
(13, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-06 08:51:48', 1),
(14, '::1', 'admin', NULL, '2021-06-07 04:46:47', 0),
(15, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-07 04:46:56', 1),
(16, '::1', 'mantap', NULL, '2021-06-07 04:48:14', 0),
(17, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-07 05:18:52', 1),
(18, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-07 06:25:05', 1),
(19, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-07 07:51:07', 1),
(20, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-07 07:53:08', 1),
(21, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-07 07:59:42', 1),
(22, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-08 07:20:19', 1),
(23, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-08 07:47:11', 1),
(24, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-08 17:16:43', 1),
(25, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-08 17:49:11', 1),
(26, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-08 18:23:44', 1),
(27, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-08 22:29:51', 1),
(28, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-09 06:06:07', 1),
(29, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-09 18:42:38', 1),
(30, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-09 22:52:44', 1),
(31, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-10 00:19:20', 1),
(32, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-10 01:31:21', 1),
(33, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-10 04:09:47', 1),
(34, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-10 08:09:12', 1),
(35, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-10 08:10:13', 1),
(36, '::1', 'karyawan', NULL, '2021-06-10 08:10:39', 0),
(37, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-10 08:21:46', 1),
(38, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-10 20:05:25', 1),
(39, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-13 01:04:24', 1),
(40, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-13 06:29:12', 1),
(41, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-13 06:42:07', 1),
(42, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-13 06:42:33', 1),
(43, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-13 06:42:56', 1),
(44, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-13 06:45:29', 1),
(45, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-13 06:46:44', 1),
(46, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-13 06:47:51', 1),
(47, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-13 06:51:03', 1),
(48, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-15 05:34:54', 1),
(49, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-15 21:09:56', 1),
(50, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-16 04:10:44', 1),
(51, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-16 08:02:45', 1),
(52, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-17 02:43:01', 1),
(53, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-20 00:55:39', 1),
(54, '::1', 'raihansulthan4@gmail.com', 1, '2021-06-21 03:35:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_permissions`
--

CREATE TABLE `auth_permissions` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_reset_attempts`
--

CREATE TABLE `auth_reset_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_tokens`
--

CREATE TABLE `auth_tokens` (
  `id` int(11) UNSIGNED NOT NULL,
  `selector` varchar(255) NOT NULL,
  `hashedValidator` varchar(255) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `expires` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_users_permissions`
--

CREATE TABLE `auth_users_permissions` (
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `permission_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cuti`
--

CREATE TABLE `cuti` (
  `id_cuti` int(11) NOT NULL,
  `nama_karyawan` varchar(255) NOT NULL,
  `nomor_karyawan` varchar(255) NOT NULL,
  `waktu_cuti` varchar(155) NOT NULL,
  `lama_cuti` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cuti`
--

INSERT INTO `cuti` (`id_cuti`, `nama_karyawan`, `nomor_karyawan`, `waktu_cuti`, `lama_cuti`, `created_at`, `updated_at`) VALUES
(5, 'Sulthan Muhammad Raihan', '11180930000075', '10 Maret - 19 Maret 2021', '14 hari', '2021-05-19 06:35:24', '2021-05-19 06:35:24'),
(10, 'Sulthan Muhammad Raihan', '11180930000075', '10 Juni - 11 Juni 2021', '3 Hari', '2021-06-10 07:55:06', '2021-06-10 07:55:06'),
(11, 'Sulthan Muhammad Raihan', '11180930000075', '14 Juni - 19 Juni 2021', '5 hari', '2021-06-10 20:10:20', '2021-06-10 20:10:20');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `nama_karyawan` varchar(255) NOT NULL,
  `nomor_karyawan` varchar(255) NOT NULL,
  `divisi` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `tanggal_masuk` varchar(255) NOT NULL,
  `tanggal_lahir` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `nama_karyawan`, `nomor_karyawan`, `divisi`, `jabatan`, `tanggal_masuk`, `tanggal_lahir`, `jenis_kelamin`, `agama`, `alamat`, `created_at`, `updated_at`) VALUES
(12, 'Sulthan Muhammad Raihan', '11180930000075', 'HR', 'Senior Staff', '2021-05-10', '1999-12-03', '', 'Islam', 'Jl. Haji Nawi', NULL, NULL),
(13, 'Muhammad Harits Daud', '11180930000062', 'HR', 'Junior Staff', '2021-05-24', '2000-03-30', 'Laki-laki', 'Islam', 'Jl. Boyong', NULL, NULL),
(20, 'Galih Alif Farizky', '11180930000063', 'Finance', 'Junior Staff', '2021-06-07', '2000-01-03', '', 'Islam', 'Jl. Lubang Buaya', NULL, NULL),
(22, 'Yasmine Amalia Ismail', '11180930000079', 'Produksi', 'Junior Staff', '2021-06-07', '2000-10-09', 'Perempuan', 'Islam', 'Jl. Cibubur Panas', NULL, NULL),
(23, 'Puja Alfina', '11180930000074', 'Produksi', 'Manager', '2021-06-07', '2000-02-15', '', 'Islam', 'Jl. Cikupa', NULL, NULL),
(24, 'Budi Purnama', '11180930000068', 'HR', 'Junior Staff', '2021-06-14', '2000-04-11', 'Laki-laki', 'Islam', 'Jl. Limo, Depok', NULL, NULL),
(25, 'Muhammad Faiz Kurniawan', '11180930000076', 'Inventory', 'Junior Staff', '2021-06-07', '2000-01-27', 'Laki-laki', 'Islam', 'Kreo', NULL, NULL),
(26, 'Rafif Izzudin Irdasyah', '11180930000065', 'Finance', 'Junior Staff', '2021-06-14', '2000-09-18', 'Laki-laki', 'Islam', 'Pasar Minggu', NULL, NULL),
(27, 'Muhammad Ali Hanif', '11180930000070', 'Produksi', 'Senior Staff', '2021-06-07', '2000-07-03', 'Laki-laki', 'Islam', 'Bangka Belitung', NULL, NULL),
(28, 'Diego Gemilang Ririhena', '11180930000071', 'Sales & Marketing', 'Manager', '2021-01-04', '1996-06-18', 'Laki-laki', 'Islam', 'Cakung', NULL, NULL),
(29, 'Nur\'Aidah Arifah', '11180930000083', 'Sales & Marketing', 'Junior Staff', '2021-06-14', '2000-01-30', 'Perempuan', 'Islam', 'Tebet, Jakarta Selatan', NULL, NULL),
(30, 'Ananta Praditya Utama', '11180930000081', 'Sales & Marketing', 'Manager', '2021-05-31', '1999-04-03', 'Laki-laki', 'Islam', 'Jatinegara, Jakarta Timur', NULL, NULL),
(31, 'Egan Anatola Rinday', '11180930000073', 'Inventory', 'Manager', '2021-05-31', '1990-03-07', 'Laki-laki', 'Islam', 'Jl. Japos', NULL, NULL),
(32, 'Ramadhan Hidayat', '11170930000070', 'Inventory', 'Senior Staff', '2021-05-31', '1998-06-09', 'Laki-laki', 'Islam', 'Ciputat, Tangerang Selatan', NULL, NULL),
(33, 'Yafi Aljafier', '11180930000066', 'Inventory', 'Junior Staff', '2021-06-01', '2000-07-21', 'Laki-laki', 'Islam', 'Ciledug', NULL, NULL),
(34, 'Salsabilla Shafa Ziema', '11180930000077', 'Sales & Marketing', 'Junior Staff', '2021-05-31', '2000-01-26', 'Perempuan', 'Islam', 'Jurangmangu Barat', NULL, NULL),
(35, 'Muhammad Taqy Pratama', '11180930000080', 'Produksi', 'Senior Staff', '2021-05-31', '2000-08-07', 'Laki-laki', 'Islam', 'Bekasi', NULL, NULL),
(36, 'Muhammad Shidqi Aulia', '11180930000090', 'Finance', 'Manager', '2021-05-31', '2000-10-17', 'Laki-laki', 'Islam', 'Depok', NULL, NULL),
(37, 'Arief Budi Nugraha', '11160930000089', 'Sales & Marketing', 'Junior Staff', '2021-06-07', '2000-01-30', 'Laki-laki', 'Islam', 'Jl. Radio Dalam', NULL, NULL),
(38, 'Varrel Aditya Setiawan', '11180930000061', 'Inventory', 'Manager', '2021-05-31', '2001-04-10', '', 'Islam', 'Cilodong', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lembur`
--

CREATE TABLE `lembur` (
  `id_lembur` int(11) NOT NULL,
  `nama_karyawan` varchar(255) NOT NULL,
  `nomor_karyawan` varchar(255) NOT NULL,
  `divisi` varchar(255) NOT NULL,
  `tanggal_lembur` date DEFAULT current_timestamp(),
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lembur`
--

INSERT INTO `lembur` (`id_lembur`, `nama_karyawan`, `nomor_karyawan`, `divisi`, `tanggal_lembur`, `created_at`, `updated_at`) VALUES
(1, 'Sulthan Muhammad Raihan', '11180930000075', 'HR', '2021-06-06', '2021-06-05 17:45:26', '2021-06-05 17:45:26'),
(2, 'Varrel Aditya Setiawan', '11180930000061', 'Sales & Marketing', '2021-06-10', '2021-06-10 02:59:31', '2021-06-10 02:59:31'),
(3, 'Galih Alif Farizky', '11180930000063', 'Finance', '2021-06-10', '2021-06-10 07:56:55', '2021-06-10 07:56:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(1, '2017-11-20-223112', 'Myth\\Auth\\Database\\Migrations\\CreateAuthTables', 'default', 'Myth\\Auth', 1622966354, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pelatihan`
--

CREATE TABLE `pelatihan` (
  `id_pelatihan` int(11) NOT NULL,
  `nama_pelatihan` varchar(255) NOT NULL,
  `waktu_pelatihan` varchar(255) NOT NULL,
  `materi_pelatihan` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pelatihan`
--

INSERT INTO `pelatihan` (`id_pelatihan`, `nama_pelatihan`, `waktu_pelatihan`, `materi_pelatihan`, `created_at`, `updated_at`) VALUES
(3, 'Pelatihan PT. Pertani', '17 Juni - 19 Juni 2021', '005 - Undangan Seluruh MHS (Raker)...pdf', '2021-06-09 06:25:08', '2021-06-09 06:25:08'),
(4, 'Pelatihan Microsoft Office', '19 Juni 2021', '1623238198_2db1d324ea48aa2c5f62.pdf', '2021-06-09 06:29:58', '2021-06-09 06:29:58'),
(5, 'Pelatihan Penanaman Kompos', '1 Juli - 5 Juli 2021', 'pre exam.pdf', '2021-06-09 18:47:16', '2021-06-09 18:47:16'),
(6, 'Berkuda untuk berkebun yang lebih efektif', '14 Juni - 21 Juni 2021', 'CV Sulthan M Raihan.pdf', '2021-06-10 04:10:58', '2021-06-10 04:10:58'),
(7, 'Pelatihan IT', '13 Juni - 15 Juni 2021', 'Cover.docx', '2021-06-10 20:09:20', '2021-06-10 20:09:20'),
(8, 'Produksi Pupuk Kompos yang Ideal', '20 Juni - 22 Juni 2021', 'modul praktikum APLIKASI SISTEM INFORMASI GEOGRAFIS.docx', '2021-06-16 04:19:49', '2021-06-16 04:19:49'),
(9, 'Pembelajaran Pemantauan Pertanian', '30 Juni - 1 Juni 2021', 'Hak dan Kewajiban Negara.docx', '2021-06-17 08:22:59', '2021-06-17 08:22:59');

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan_karyawan`
--

CREATE TABLE `penerimaan_karyawan` (
  `id_penerimaan` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tanggal_lahir` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `divisi` varchar(255) NOT NULL,
  `pengalaman` varchar(255) NOT NULL,
  `no_telepon` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `cv` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penerimaan_karyawan`
--

INSERT INTO `penerimaan_karyawan` (`id_penerimaan`, `nama`, `tanggal_lahir`, `email`, `divisi`, `pengalaman`, `no_telepon`, `alamat`, `cv`, `created_at`, `updated_at`) VALUES
(1, 'Naufal Ramadhan', '20-12-1999', 'gopalrambo@gmail.com', 'Finance', 'Nyari duit banyak', '0811371947', 'Jl. Radio Dalam', 'Ini CVku mana CVmu?', '2021-05-16 01:54:14', '2021-05-16 01:54:14'),
(3, 'Naufal Muhammad Thufail', '30 April 1998', 'naufalmthufail@gmail.com', 'Finance', 'Magang di OJK', '0819347721', 'Jl. Vila Dago', 'Mantap', '2021-05-19 01:05:21', '2021-05-19 01:05:21'),
(4, 'Nadila Humaira', '18 Desember 1998', 'nadilahumaira@gmail.com', 'HR', 'Menjadi asisten personalia', '081947184', 'Jl. Serpong Tandus Banget', 'Mantap', '2021-05-19 01:11:19', '2021-05-19 01:11:19'),
(10, 'Muhammad Ashry Rivaldi', '1999-10-20', 'ashryrivaldi@gmail.com', 'Produksi', 'Memproduksi sepatu sendiri', '085814198', 'Jl. Ciledug', 'Mantap', '2021-05-19 02:10:41', '2021-05-19 02:10:41'),
(14, 'Rafi Nabil Muktabar', '2000-01-02', 'rafinabil@gmail.com', 'Purchasing', 'Membeli banyak barang', '08581947197', 'Jl. di Kedoya', 'Cakep', '2021-05-19 05:21:40', '2021-05-19 05:21:40'),
(17, 'Muhammad Fathur', '1996-11-07', 'rastaman@gmail.com', 'Finance', 'Fresh Graduate', '0819519469', 'Jl. Kesana', '1622960521_d0303b9450a4ee798c85.pdf', '2021-06-06 01:22:01', '2021-06-06 01:22:01'),
(19, 'Aditya Rafi', '1999-08-19', 'raficoeg@gmail.com', 'Finance', 'Nilep Duit', '078194197', 'Jl. Haji Lahir', '1623191186_c22a2d0a2c79b23bc41c.pdf', '2021-06-08 17:26:26', '2021-06-08 17:26:26'),
(20, 'Muhammad Rizky Aufan', '1999-04-13', 'aufanjr@gmail.com', 'HR', 'Oprec kengurus himpunan', '082915719', 'Jl. Saidi', '1623191317_f5ac82d7d6cd96514623.pdf', '2021-06-08 17:28:37', '2021-06-08 17:28:37');

-- --------------------------------------------------------

--
-- Table structure for table `penggajian`
--

CREATE TABLE `penggajian` (
  `id_gaji` int(11) NOT NULL,
  `nomor_karyawan` varchar(255) NOT NULL,
  `nama_karyawan` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `divisi` varchar(255) NOT NULL,
  `total_gaji` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penggajian`
--

INSERT INTO `penggajian` (`id_gaji`, `nomor_karyawan`, `nama_karyawan`, `jabatan`, `divisi`, `total_gaji`, `created_at`, `updated_at`) VALUES
(1, '11180930000061', 'Varrel Aditya Setiawan', 'Senior Staff', 'Sales & Marketing', '8,000,000', '2021-06-05 18:57:17', '2021-06-05 18:57:17'),
(2, '11180930000079', 'Muhammad Alfarizy', 'Senior Staff', 'Purchasing', '6,000,000', '2021-06-05 19:02:31', '2021-06-05 19:02:31'),
(3, '11180930000063', 'Galih Alif Farizky', 'Senior Staff', 'Finance', '8.000.000', '2021-06-10 20:08:45', '2021-06-10 20:08:45');

-- --------------------------------------------------------

--
-- Table structure for table `pengunduran_diri`
--

CREATE TABLE `pengunduran_diri` (
  `id_pengunduran_diri` int(11) NOT NULL,
  `nomor_karyawan` varchar(255) NOT NULL,
  `nama_karyawan` varchar(255) NOT NULL,
  `alasan` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengunduran_diri`
--

INSERT INTO `pengunduran_diri` (`id_pengunduran_diri`, `nomor_karyawan`, `nama_karyawan`, `alasan`, `created_at`, `updated_at`) VALUES
(2, '11180930000090', 'Achmad Shiddqi Aulia', 'Ingin keliling dunia mencari pengalaman kerja yang lebih baik', '2021-05-16 05:11:21', '2021-05-16 05:11:21'),
(3, '11180930000082', 'Ananta Praditya Utama', 'Karena ingin membuat startup', '2021-05-16 08:00:41', '2021-05-16 08:00:41'),
(4, '11180930000076', 'Muhammad Faiz Kurniawan', 'Ingin merintis perusahaan', '2021-06-10 07:55:44', '2021-06-10 07:55:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `reset_hash` varchar(255) DEFAULT NULL,
  `reset_at` datetime DEFAULT NULL,
  `reset_expires` datetime DEFAULT NULL,
  `activate_hash` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `status_message` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `force_pass_reset` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password_hash`, `reset_hash`, `reset_at`, `reset_expires`, `activate_hash`, `status`, `status_message`, `active`, `force_pass_reset`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'raihansulthan4@gmail.com', 'admin', '$2y$10$KDIQOo7ASnk0YwxHGSKyN.qmIAka1rVlduqFmrG.t8f2168rqgzuO', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-06-06 03:32:59', '2021-06-06 03:32:59', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id_absensi`);

--
-- Indexes for table `auth_activation_attempts`
--
ALTER TABLE `auth_activation_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_groups`
--
ALTER TABLE `auth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_groups_permissions`
--
ALTER TABLE `auth_groups_permissions`
  ADD KEY `auth_groups_permissions_permission_id_foreign` (`permission_id`),
  ADD KEY `group_id_permission_id` (`group_id`,`permission_id`);

--
-- Indexes for table `auth_groups_users`
--
ALTER TABLE `auth_groups_users`
  ADD KEY `auth_groups_users_user_id_foreign` (`user_id`),
  ADD KEY `group_id_user_id` (`group_id`,`user_id`);

--
-- Indexes for table `auth_logins`
--
ALTER TABLE `auth_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_reset_attempts`
--
ALTER TABLE `auth_reset_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auth_tokens_user_id_foreign` (`user_id`),
  ADD KEY `selector` (`selector`);

--
-- Indexes for table `auth_users_permissions`
--
ALTER TABLE `auth_users_permissions`
  ADD KEY `auth_users_permissions_permission_id_foreign` (`permission_id`),
  ADD KEY `user_id_permission_id` (`user_id`,`permission_id`);

--
-- Indexes for table `cuti`
--
ALTER TABLE `cuti`
  ADD PRIMARY KEY (`id_cuti`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id_karyawan`,`nama_karyawan`);

--
-- Indexes for table `lembur`
--
ALTER TABLE `lembur`
  ADD PRIMARY KEY (`id_lembur`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pelatihan`
--
ALTER TABLE `pelatihan`
  ADD PRIMARY KEY (`id_pelatihan`);

--
-- Indexes for table `penerimaan_karyawan`
--
ALTER TABLE `penerimaan_karyawan`
  ADD PRIMARY KEY (`id_penerimaan`);

--
-- Indexes for table `penggajian`
--
ALTER TABLE `penggajian`
  ADD PRIMARY KEY (`id_gaji`);

--
-- Indexes for table `pengunduran_diri`
--
ALTER TABLE `pengunduran_diri`
  ADD PRIMARY KEY (`id_pengunduran_diri`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id_absensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `auth_activation_attempts`
--
ALTER TABLE `auth_activation_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_groups`
--
ALTER TABLE `auth_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_logins`
--
ALTER TABLE `auth_logins`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_reset_attempts`
--
ALTER TABLE `auth_reset_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cuti`
--
ALTER TABLE `cuti`
  MODIFY `id_cuti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id_karyawan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `lembur`
--
ALTER TABLE `lembur`
  MODIFY `id_lembur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pelatihan`
--
ALTER TABLE `pelatihan`
  MODIFY `id_pelatihan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `penerimaan_karyawan`
--
ALTER TABLE `penerimaan_karyawan`
  MODIFY `id_penerimaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `penggajian`
--
ALTER TABLE `penggajian`
  MODIFY `id_gaji` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pengunduran_diri`
--
ALTER TABLE `pengunduran_diri`
  MODIFY `id_pengunduran_diri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_groups_permissions`
--
ALTER TABLE `auth_groups_permissions`
  ADD CONSTRAINT `auth_groups_permissions_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_groups_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `auth_permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_groups_users`
--
ALTER TABLE `auth_groups_users`
  ADD CONSTRAINT `auth_groups_users_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_groups_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  ADD CONSTRAINT `auth_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_users_permissions`
--
ALTER TABLE `auth_users_permissions`
  ADD CONSTRAINT `auth_users_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `auth_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_users_permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
