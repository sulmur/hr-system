<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->group('', ['filter' => 'login'], function($routes) {
	$routes->get('/admin', 'Admin\Admin::index');
});

$routes->get('/', 'Home::index');
// $routes->get('/login', 'Page::login');
// $routes->get('/dashboard', 'Page::dashboard');


// Admin Role
$routes->get('/list-absensi-lembur', 'Admin\Admin::list_absensi_lembur');
$routes->get('/data-karyawan', 'Admin\DataKaryawan::data_karyawan');
$routes->get('/data-karyawan/(:any)', 'Admin\DataKaryawan::detail_data_karyawan/$1');
$routes->delete('/data-karyawan/(:num)', 'Admin\DataKaryawan::delete/$1');
// $routes->get('/detail-data-karyawan/(:segment)', 'Admin::detail_data_karyawan/$1');

$routes->get('/tambah-data-karyawan', 'Admin\DataKaryawan::tambah_data_karyawan');
$routes->get('/form-penggajian', 'Admin\Penggajian::form_penggajian');
$routes->get('/cuti', 'Admin\Admin::cuti');
$routes->get('/pengunduran-diri', 'Admin\Admin::pengunduran_diri');
$routes->get('/list-calon-karyawan', 'Admin\PenerimaanKaryawan::list_calon_karyawan');
$routes->get('/tambah-pelatihan', 'Admin\TambahPelatihan::tambah_pelatihan');
$routes->get('/laporan', 'Admin\Admin::laporan');

// Manager Role 
$routes->get('/manager', 'Manager\Manager::index');
$routes->get('/list-absensi-lembur-manager', 'Manager\Manager::list_absensi_lembur_manager');
$routes->get('/data-karyawan-manager', 'Manager\Manager::data_karyawan_manager');
$routes->get('/laporan-manager', 'Manager\Manager::laporan_manager');
$routes->get('/penggajian-manager', 'Manager\Penggajian::penggajian_manager');
$routes->get('/pengunduran-diri-manager', 'Manager\PengunduranDiri::pengunduran_diri_manager');
$routes->get('/cuti-manager', 'Manager\Cuti::cuti_manager');
$routes->get('/pelatihan-manager', 'Manager\Manager::pelatihan_manager');
$routes->get('/penerimaan-calon-karyawan', 'Manager\PenerimaanKaryawan::penerimaan_calon_karyawan');

// Karyawan Role
$routes->get('/karyawan', 'Karyawan\Karyawan::index');
$routes->get('/absensi-lembur', 'Karyawan\Karyawan::absensi_lembur');
$routes->get('/history-cuti', 'Karyawan\Cuti::history_cuti');
$routes->get('/tambah-cuti', 'Karyawan\Cuti::tambah_cuti');
$routes->get('/history-pengunduran-diri', 'Karyawan\PengunduranDiri::history_pengunduran_diri');
$routes->get('/tambah-pengunduran-diri', 'Karyawan\PengunduranDiri::tambah_pengunduran_diri');
$routes->get('/pelatihan', 'Karyawan\Pelatihan::pelatihan');

// Finance Role
$routes->get('/finance', 'Finance\Finance::index');
$routes->get('/penggajian-finance', 'Finance\Finance::penggajian');
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
