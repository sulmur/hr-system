<?php

namespace App\Models;

use CodeIgniter\Model;

class PengunduranModel extends Model
{
    protected $table = 'pengunduran_diri';
    protected $primaryKey = 'id_pengunduran_diri';
    protected $useTimestamps = true;
    protected $allowedFields = ['nama_karyawan', 'nomor_karyawan', 'alasan'];

    public function __construct()
    {
        $this->db = db_connect();
        $this->builder = $this->db->table($this->table);
    }

    public function getPengunduran() 
    {
        return $this->builder->get();
    }

    public function total_pengunduran()
    {
        return $this->db->table('pengunduran_diri')->countAll();
    }

    public function hapus($id_pengunduran_diri)
    {
        return $this->builder->delete(['id_pengunduran_diri' => $id_pengunduran_diri]);
    }
}