<?php

namespace App\Models;

use CodeIgniter\Model;

class KaryawanModel extends Model
{
    protected $table = 'karyawan';
    protected $useTimestamps = true;
    protected $primaryKey = 'id_karyawan';
    protected $allowedFields = ['nama_karyawan', 'nomor_karyawan', 'divisi', 'jabatan', 'jenis_kelamin', 'tanggal_masuk', 'tanggal_lahir', 'agama', 'alamat'];

    public function __construct()
	{
		$this->db = db_connect();
        $this->builder = $this->db->table($this->table);
	}

    public function getKaryawan()
    {
        return $this->builder->get();
    }

    public function getDataById($id)
    {
        $this->builder->where('id_karyawan', $id);

        return $this->builder->get()->getRowArray();
    }

    public function tambah($data)
    {
        return $this->builder->insert($data);
    }

    public function hapus($id_karyawan)
    {
        $this->where(['id_karyawan' => $id_karyawan]);
        $this->builder->get()->getResultArray();
        return $this->builder->delete(['id_karyawan' => $id_karyawan]);
    }

    public function ubah($data, $id)
    {
        return $this->builder->update($data, ['id_karyawan' => $id]);
    }

}