<?php

namespace App\Models;

use CodeIgniter\Model;

class PenggajianModel extends Model
{
    protected $table = 'penggajian';
    protected $primaryKey = 'id_gaji';
    protected $useTimestamps = true;
    protected $allowedFields = ['nama_karyawan', 'nomor_karyawan', 'jabatan', 'divisi', 'total_gaji'];


    public function getPenggajian($id_gaji = false) 
    {
        if($id_gaji == false) {
            return $this->findAll();
        }

        return $this->where(['id_gaji' => $id_gaji])->first();
    }
}