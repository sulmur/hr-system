<?php

namespace App\Models;

use CodeIgniter\Model;

class CutiModel extends Model
{
    protected $table = 'cuti';
    protected $primaryKey = 'id_cuti';
    protected $useTimestamps = true;
    protected $allowedFields = ['nama_karyawan', 'nomor_karyawan', 'waktu_cuti', 'lama_cuti'];


    public function __construct()
    {
        $this->db = db_connect();
        $this->builder = $this->db->table($this->table);
    }
    
    public function getCuti() 
    {
        return $this->builder->get();
    }

    function tampil_cuti()
    {
        $this->db->select('id_cuti, nama_karyawan, COUNT(nomor_karyawan) as total');
        $this->db->group_by('nomor_karyawan'); 
        $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('cuti');
        return $hasil;
    }

    public function total_cuti()
    {
        return $this->db->table('cuti')->countAll();
    }

    public function hapus($id_cuti)
    {
        return $this->builder->delete(['id_cuti' => $id_cuti]);
    }
}