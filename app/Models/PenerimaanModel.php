<?php

namespace App\Models;

use CodeIgniter\Model;

class PenerimaanModel extends Model
{
    protected $DBGroup = 'default';
    protected $table = 'penerimaan_karyawan';
    protected $primaryKey = 'id_penerimaan';
    protected $returnType = 'object';
    protected $useTimestamps = true;
    protected $allowedFields = ['nama', 'tanggal_lahir', 'email', 'divisi', 'pengalaman', 'no_telepon', 'alamat', 'cv'];

    public function __construct()
    {
        $this->db = db_connect();
        $this->builder = $this->db->table($this->table);
    }

    public function getPenerimaan() 
    {
        return $this->builder->get();
    }

    // public function hapus($id_penerimaan)
    // {
    //     return $this->builder->delete(['id_penerimaan' => $id_penerimaan]);
    // }

    public function hapus($id_penerimaan)
    {
        $this->where(['id_penerimaan' => $id_penerimaan]);
        $this->builder->get()->getResultArray();
        return $this->builder->delete(['id_penerimaan' => $id_penerimaan]);
    }
}