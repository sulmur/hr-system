<?php

namespace App\Models;

use CodeIgniter\Model;

class LemburModel extends Model
{
    protected $table = 'lembur';
    protected $primaryKey = 'id_lembur';
    protected $useTimestamps = true;
    protected $allowedFields = ['nama_karyawan', 'nomor_karyawan', 'divisi', 'waktu_lembur'];


    public function getLembur($id_lembur = false) 
    {
        if($id_lembur == false) {
            return $this->findAll();
        }

        return $this->where(['id_lembur' => $id_lembur])->first();
    }
}