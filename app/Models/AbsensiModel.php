<?php

namespace App\Models;

use CodeIgniter\Model;

class AbsensiModel extends Model
{
    protected $table = 'absensi';
    protected $primaryKey = 'id_absensi';
    protected $useTimestamps = true;
    protected $allowedFields = ['nama_karyawan', 'nomor_karyawan', 'divisi', 'waktu', 'tanggal'];


    public function getAbsensi($id_absensi = false) 
    {
        if($id_absensi == false) {
            return $this->findAll();
        }

        return $this->where(['id_absensi' => $id_absensi])->first();
    }
}