<?php

namespace App\Models;

use CodeIgniter\Model;

class PelatihanModel extends Model
{
    protected $DBGroup = 'default';
    protected $table = 'pelatihan';
    protected $primaryKey = 'id_pelatihan';
    protected $returnType = 'object';
    protected $useTimestamps = true;
    protected $allowedFields = ['nama_pelatihan', 'waktu_pelatihan', 'materi_pelatihan'];

    // public function __construct()
    // {
    //     $this->db = db_connect();
    //     $this->builder = $this->db->table($this->table);
    // }

    public function getPelatihan($id_pelatihan = false) 
    {
        if($id_pelatihan == false) {
            return $this->findAll();
        }

        return $this->where(['id_pelatihan' => $id_pelatihan])->first();
    }

    public function total_pelatihan()
    {
        return $this->db->table('pelatihan')->countAll();
    }
}