<?php namespace App\Controllers\Admin;


use App\Controllers\BaseController;
use App\Models\KaryawanModel;

class DataKaryawan extends BaseController
{
	// protected $karyawanModel;

	public function __construct()
	{
		$this->session = service('session');
		$this->auth = service('authentication');

		$this->karyawanModel = new KaryawanModel;
	}

	public function data_karyawan()
	{
		if (!$this->auth->check())
		{
			$redirectURL = session('redirect_url') ?? '/';
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
		}

		$data = [
			'title' => 'Data Karyawan | PT. Pertani',
			'karyawan' => $this->karyawanModel->getKaryawan(),
			'validation' => \Config\Services::validation()
		];

		return view("admin/data_karyawan/data_karyawan", $data);
	}

	public function tambah()
	{
		// validasi input
		if(!$this->validate([
			'nama_karyawan' => [
				'label' => 'Nama karyawan',
				'rules' => 'required',				
			],
			'nomor_karyawan' => [
				'label' => 'Nomor karyawan',
				'rules' => 'required|numeric|is_unique[karyawan.nomor_karyawan]',
			],
			'divisi' => [
				'label' => 'Divisi',
				'rules' => 'required'
			],
			'jabatan' => [
				'label' => 'Jabatan',
				'rules' => 'required'
			],
			'tanggal_masuk' => [
				'label' => 'Tanggal masuk',
				'rules' => 'required'
			],
			'tanggal_lahir' => [
				'label' => 'Tanggal lahir',
				'rules' => 'required'
			],
			'jenis_kelamin' => [
				'label' => 'Jenis kelamin',
				'rules' => 'required'
			],
			'agama' => [
				'label' => 'Agama',
				'rules' => 'required'
			],
			'alamat' => [
				'label' => 'Alamat',
				'rules' => 'required'
			]
		])) {
			$validation = \Config\Services::validation();
			session()->setFlashdata('error', \Config\Services::validation()->listErrors());
			return redirect()->to('/data-karyawan')->withInput()->with('validation', $validation);
		}

		$data = [
			'nama_karyawan' => $this->request->getPost('nama_karyawan'),
			'nomor_karyawan' => $this->request->getPost('nomor_karyawan'),
			'divisi' => $this->request->getPost('divisi'),
			'jabatan' => $this->request->getPost('jabatan'),
			'tanggal_masuk' => $this->request->getPost('tanggal_masuk'),
			'tanggal_lahir' => $this->request->getPost('tanggal_lahir'),
			'jenis_kelamin' => $this->request->getPost('jenis_kelamin'),
			'agama' => $this->request->getPost('agama'),
			'alamat' => $this->request->getPost('alamat'),
		];

		$success = $this->karyawanModel->tambah($data);
		if ($success) {
			session()->setFlashdata('pesan', 'Data karyawan berhasil ditambahkan');
			return redirect()->to('/data-karyawan');
		}
	}	

	public function ubah()
	{
		$id = $this->request->getPost('id_karyawan');

		$data = [
			'nama_karyawan' => $this->request->getPost('nama_karyawan'),
			'nomor_karyawan' => $this->request->getPost('nomor_karyawan'),
			'divisi' => $this->request->getPost('divisi'),
			'jabatan' => $this->request->getPost('jabatan'),
			'tanggal_masuk' => $this->request->getPost('tanggal_masuk'),
			'tanggal_lahir' => $this->request->getPost('tanggal_lahir'),
			'jenis_kelamin' => $this->request->getPost('jenis_kelamin'),
			'agama' => $this->request->getPost('agama'),
			'alamat' => $this->request->getPost('alamat'),
		];

		$success = $this->karyawanModel->ubah($data, $id);
		if ($success) {
			session()->setFlashdata('pesan', 'Data karyawan berhasil diubah');
			return redirect()->to('/data-karyawan');
		}
	}	

	public function hapus($id_karyawan)
	{
		$success = $this->karyawanModel->hapus($id_karyawan);
		if ($success) {
			session()->setFlashdata('pesan', 'Data karyawan berhasil dihapus');
			return redirect()->to('/data-karyawan');
		}
	}
}