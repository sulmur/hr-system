<?php namespace App\Controllers\Admin;


use App\Controllers\BaseController;
use App\Models\PenerimaanModel;

class PenerimaanKaryawan extends BaseController
{
	protected $penerimaanModel;

	public function __construct()
	{
		$this->penerimaanModel = new PenerimaanModel();
	}

	public function list_calon_karyawan()
	{
		$data = [
			'title' => 'List Calon Karyawan | PT. Pertani',
			'penerimaan' => $this->penerimaanModel->getPenerimaan()
		];
		return view("admin/list_calon_karyawan", $data);
	}
    	
}