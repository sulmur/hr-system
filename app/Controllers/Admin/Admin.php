<?php namespace App\Controllers\Admin;


use App\Controllers\BaseController;
use App\Models\KaryawanModel;
use App\Models\PenerimaanModel;
use App\Models\CutiModel;
use App\Models\PengunduranModel;
use App\Models\PelatihanModel;
use App\Models\AbsensiModel;
use App\Models\LemburModel;

class Admin extends BaseController
{
	protected $cutiModel;
	protected $pengunduranModel;
	protected $penerimaanModel;
	protected $pelatihanModel;
	protected $absensiModel;
	protected $lemburModel;
	protected $karyawanModel;

	public function __construct()
	{
		$this->session = service('session');
		$this->auth = service('authentication');

		$this->karyawanModel = new KaryawanModel();
		$this->cutiModel = new CutiModel();
		$this->pengunduranModel = new PengunduranModel();
		$this->absensiModel = new AbsensiModel();
		$this->lemburModel = new LemburModel();
		$this->pelatihanModel = new PelatihanModel();
	}

	public function index()
	{
		if (!$this->auth->check())
		{
			$redirectURL = session('redirect_url') ?? '/';
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
		}

		$data = [
			'title' => 'Dashboard | PT. Pertani',
			'karyawan' => $this->karyawanModel->getKaryawan(),
			'cuti' => $this->cutiModel->getCuti(),
			'pengunduran' => $this->pengunduranModel->getPengunduran(),
			'pelatihan' => $this->pelatihanModel->getPelatihan(),
			'total_cuti' => $this->cutiModel->total_cuti(),
			'total_pengunduran' => $this->pengunduranModel->total_pengunduran(),
			'total_pelatihan' => $this->pelatihanModel->total_pelatihan()
		];
		return view("admin/index", $data);
	}

	public function cuti()
	{
		if (!$this->auth->check())
		{
			$redirectURL = session('redirect_url') ?? '/';
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
		}

		$data = [
			'title' => 'List Permohonan Cuti | PT. Pertani',
			'cuti' => $this->cutiModel->getCuti()
		];
		return view("admin/cuti", $data);
	}

	public function pengunduran_diri()
	{
		if (!$this->auth->check())
		{
			$redirectURL = session('redirect_url') ?? '/';
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
		}

		$data = [
			'title' => 'List Pengunduran Diri | PT. Pertani',
			'pengunduran' => $this->pengunduranModel->getPengunduran()
		];
		return view("admin/pengunduran_diri", $data);
	}

	public function list_absensi_lembur()
	{
		if (!$this->auth->check())
		{
			$redirectURL = session('redirect_url') ?? '/';
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
		}

		$data = [
			'title' => 'List Absensi & Lembur | PT. Pertani',
			'absensi' => $this->absensiModel->getAbsensi(),
			'lembur' => $this->lemburModel->getLembur()
		];
		return view("admin/list_absensi_lembur", $data);
	}

	public function laporan()
	{
		if (!$this->auth->check())
		{
			$redirectURL = session('redirect_url') ?? '/';
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
		}
		
		$data = [
			'title' => 'Laporan | PT. Pertani',
		];
		return view("admin/laporan", $data);
	}
}