<?php namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\PenggajianModel;

class Penggajian extends BaseController
{
    protected $penggajianModel;

	public function __construct()
	{
		$this->session = service('session');
		$this->auth = service('authentication');

		$this->penggajianModel = new PenggajianModel();
	}

	public function form_penggajian()
	{
		if (!$this->auth->check())
		{
			$redirectURL = session('redirect_url') ?? '/';
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
		}

		$data = [
			'title' => 'Form Penggajian | PT. Pertani',
			'validation' => \Config\Services::validation()
		];
		return view("admin/form_penggajian", $data);
	}

    public function save()
	{
		// validasi input
		if(!$this->validate([
			'nama_karyawan' => [
                'label' => 'Nama karyawan',
				'rules' => 'required',
			],
			'nomor_karyawan' => [
                'label' => 'Nomor karyawan',
				'rules' => 'required',
            ],
            'jabatan' => [
                'label' => 'Jabatan',
                'rules' => 'required'
            ],
            'divisi' => [
                'label' => 'Divisi',
				'rules' => 'required',
            ],
            'total_gaji' => [
                'label' => 'Total gaji',
                'rules' => 'required'
            ]
		])) {
			$validation = \Config\Services::validation();
			return redirect()->to('/form-penggajian')->withInput()->with('validation', $validation);
		}

		$this->penggajianModel->save([
			'nama_karyawan' => $this->request->getVar('nama_karyawan'),
			'nomor_karyawan' => $this->request->getVar('nomor_karyawan'),
            'jabatan' => $this->request->getVar('jabatan'),
			'divisi' => $this->request->getVar('divisi'),
            'total_gaji' => $this->request->getVar('total_gaji')
		]);

		session()->setFlashdata('pesan', 'Penggajian berhasil ditambahkan');

		return redirect()->to('/form-penggajian');
	}
}