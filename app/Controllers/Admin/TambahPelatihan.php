<?php namespace App\Controllers\Admin;


use App\Controllers\BaseController;
use App\Models\PelatihanModel;

class TambahPelatihan extends BaseController
{
	protected $pelatihanModel;

	public function __construct()
	{
		$this->session = service('session');
		$this->auth = service('authentication');
		
		$this->pelatihanModel = new PelatihanModel();
	}

	public function tambah_pelatihan()
	{
		if (!$this->auth->check())
		{
			$redirectURL = session('redirect_url') ?? '/';
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
		}

		$data = [
			'title' => 'Tambah Pelatihan Karyawan | PT. Pertani',
			'pelatihan' => $this->pelatihanModel->getPelatihan(),
			'validation' => \Config\Services::validation()
		];
		return view("admin/tambah_pelatihan", $data);
	} 

	public function save()
	{
		// validasi input
		if(!$this->validate([
			'nama_pelatihan' => [
				'label' => 'Nama pelatihan',
				'rules' => 'required'
			],
			'waktu_pelatihan' => [
				'label' => 'Waktu pelatihan',
				'rules' => 'required'
			],
			'materi_pelatihan' => [
				'label' => 'Materi pelatihan',
				'rules' => 'uploaded[materi_pelatihan]'
			]
		])) {
			// $validation = \Config\Services::validation();
			return redirect()->to('/tambah-pelatihan')->withInput();
		}

		// ambil file
		$fileMateri = $this->request->getFile('materi_pelatihan');
		// generate nama cv random
		// $namaMateri = $fileMateri->getRandomName();
		// ambil nama file cv
		$namaMateri = $fileMateri->getName();
		//pindahkan file ke folder
		$fileMateri->move('assets/file');

		$this->pelatihanModel->save([
			'nama_pelatihan' => $this->request->getVar('nama_pelatihan'),
			'waktu_pelatihan' => $this->request->getVar('waktu_pelatihan'),
			'materi_pelatihan' => $namaMateri
		]);

		session()->setFlashdata('pesan', 'Pelatihan berhasil ditambahkan');

		return redirect()->to('/tambah-pelatihan');
	}

// public function download($id_pelatihan)
// 	{
// 		$pelatihan = new PelatihanModel();
// 		$data = $pelatihan->find($id_pelatihan);
// 		return $this->response->download('assets/file/' . $data->pelatihan, null);
// 	}
	
}