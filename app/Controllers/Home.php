<?php

namespace App\Controllers;

use App\Models\PenerimaanModel;

class Home extends BaseController
{
	protected $penerimaanModel;

	public function __construct()
	{
		$this->penerimaanModel = new PenerimaanModel();
	}

	public function index()
	{
		// session();
		$data = [
			'title' => 'Penerimaan Pegawai | PT. Pertani',
			'penerimaan' => $this->penerimaanModel->getPenerimaan(),
			'validation' => \Config\Services::validation()
		];
		return view('index', $data);
	}

	public function save()
	{
		// validasi input
		if(!$this->validate([
			'nama' => [
				'label' => 'Nama',
				'rules' => 'required'
			],
			'tanggal_lahir' => [
				'label' => 'Tanggal lahir',
				'rules' => 'required'
			],
			'email' => [
				'label' => 'Email',
				'rules' => 'required|is_unique[penerimaan_karyawan.email]'
			],
			'divisi' => [
				'label' => 'Divisi',
				'rules' => 'required'
			],
			'no_telepon' => [
				'label' => 'Nomor telepon',
				'rules' => 'required',
			],
			'pengalaman' => [
				'label'=> 'Pengalaman',
				'rules' => 'required'
			],
			'alamat' => [
				'label' => 'Alamat',
				'rules' => 'required'
			],
			'cv' => [
				'label' => 'CV',
				'rules' => 'uploaded[cv]|max_size[cv, 32012]',
			]
		])) {
			// $validation = \Config\Services::validation();
			// return redirect()->to('/')->withInput()->with('validation', $validation);
			return redirect()->to('/#form')->withInput();
		}

		// ambil file
		$fileCv = $this->request->getFile('cv');
		// generate nama cv random
		// $namaCv = $fileCv->getRandomName();
		// ambil nama file cv
		$namaCv = $fileCv->getName();
		//pindahkan file ke folder
		$fileCv->move('assets/file');

		$this->penerimaanModel->save([
			'nama' => $this->request->getVar('nama'),
			'tanggal_lahir' => $this->request->getVar('tanggal_lahir'),
			'email' => $this->request->getVar('email'),
			'divisi' => $this->request->getVar('divisi'),
			'no_telepon' => $this->request->getVar('no_telepon'),
			'pengalaman' => $this->request->getVar('pengalaman'),
			'alamat' => $this->request->getVar('alamat'),
			'cv' => $namaCv
		]);

		session()->setFlashdata('pesan', 'Pendaftaran berhasil');

		return redirect()->to('/');
	}
}
