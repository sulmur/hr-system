<?php

namespace App\Controllers\Finance;

use App\Controllers\BaseController;
use App\Models\PenggajianModel;
use App\Models\CutiModel;
use App\Models\PengunduranModel;
use App\Models\PelatihanModel;

class Finance extends BaseController
{
	protected $penggajianModel;

	public function __construct()
	{
		$this->penggajianModel = new PenggajianModel();
		$this->cutiModel = new CutiModel();
		$this->pengunduranModel = new PengunduranModel();
		$this->pelatihanModel = new PelatihanModel();
	}

	public function index()
	{
		$data = [
			'title' => 'Dashboard HR Finance | PT. Pertani',
			'total_cuti' => $this->cutiModel->total_cuti(),
			'cuti' => $this->cutiModel->getCuti(),
			'pengunduran' => $this->pengunduranModel->getPengunduran(),
			'pelatihan' => $this->pelatihanModel->getPelatihan(),
			'total_pengunduran' => $this->pengunduranModel->total_pengunduran(),
			'total_pelatihan' => $this->pelatihanModel->total_pelatihan()
		];
		return view("finance/index", $data);
	}

	public function penggajian()
	{
		$data = [
			'title' => 'Penggajian Finance | PT. Pertani',
			'penggajian' => $this->penggajianModel->getPenggajian()
		];
		return view("finance/penggajian", $data);
	}
}
