<?php namespace App\Controllers\Karyawan;

use App\Controllers\BaseController;
use App\Models\LemburModel;

class Lembur extends BaseController
{
    protected $lemburModel;

	public function __construct()
	{
		$this->lemburModel = new LemburModel();
	}

    public function save()
	{
		// validasi input
		if(!$this->validate([
			'nama_karyawan' => [
                'label' => 'Nama karyawan',
				'rules' => 'required',
			],
			'nomor_karyawan' => [
                'label' => 'Nomor karyawan',
				'rules' => 'required',
            ],
            'divisi' => [
                'label' => 'Divisi',
				'rules' => 'required',
            ],
		])) {
			$validation = \Config\Services::validation();
			return redirect()->to('/absensi-lembur')->withInput()->with('validation', $validation);
		}

		$this->lemburModel->save([
			'nama_karyawan' => $this->request->getVar('nama_karyawan'),
			'nomor_karyawan' => $this->request->getVar('nomor_karyawan'),
			'divisi' => $this->request->getVar('divisi'),
            'tanggal_lembur' => $this->request->getVar('tanggal_lembur')
		]);

		session()->setFlashdata('pesan', 'Lembur berhasil ditambahkan');

		return redirect()->to('/absensi-lembur');
	}
}