<?php namespace App\Controllers\Karyawan;

use App\Controllers\BaseController;
use App\Models\AbsensiModel;
use App\Models\LemburModel;
use App\Models\CutiModel;
use App\Models\PengunduranModel;
use App\Models\PelatihanModel;

class Karyawan extends BaseController
{
	protected $absensiModel;
	protected $lemburModel;
	protected $pelatihanModel;
	protected $cutiModel;
	protected $pengunduranModel;

	public function __construct()
	{
		$this->absensiModel = new AbsensiModel();
		$this->lemburModel = new LemburModel();
		$this->cutiModel = new CutiModel();
		$this->pengunduranModel = new PengunduranModel();
		$this->pelatihanModel = new PelatihanModel();
	}

	public function index()
	{
		$data = [
			'title' => 'Dashboard Karyawan | PT. Pertani',
			'cuti' => $this->cutiModel->getCuti(),
			'pengunduran' => $this->pengunduranModel->getPengunduran(),
			'pelatihan' => $this->pelatihanModel->getPelatihan(),
			'total_cuti' => $this->cutiModel->total_cuti(),
			'total_pengunduran' => $this->pengunduranModel->total_pengunduran(),
			'total_pelatihan' => $this->pelatihanModel->total_pelatihan()
		];
		return view("karyawan/index", $data);
	}

	public function absensi_lembur()
	{
		$data = [
			'title' => 'Absensi & Lembur | PT. Pertani',
            'absensi' => $this->absensiModel->getAbsensi(),
			'lembur' => $this->lemburModel->getLembur(),
            'validation' => \Config\Services::validation()
		];
		return view("karyawan/absensi_lembur", $data);
	}
}