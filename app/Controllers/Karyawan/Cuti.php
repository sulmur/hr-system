<?php namespace App\Controllers\Karyawan;

use App\Controllers\BaseController;
use App\Models\CutiModel;

class Cuti extends BaseController
{
    protected $cutiModel;

	public function __construct()
	{
		$this->cutiModel = new CutiModel();
	}

    public function tambah_cuti()
	{
		// session();
		$data = [
			'title' => 'Tambah Cuti | PT. Pertani',
            'cuti' => $this->cutiModel->getCuti(),
			'validation' => \Config\Services::validation()
		];
		return view("karyawan/tambah_cuti", $data);
	}

	public function history_cuti()
	{
		$data = [
			'title' => 'History Cuti | PT. Pertani',
            'cuti' => $this->cutiModel->getCuti()
		];
		return view("karyawan/history_cuti", $data);
	}

    public function save()
	{
		// validasi input
		if(!$this->validate([
			'waktu_cuti' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'waktu cuti harus diisi.'
				]
			],
			'lama_cuti' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'lama cuti harus diisi.'
				] 
			]
		])) {
			$validation = \Config\Services::validation();
			return redirect()->to('/tambah-cuti')->withInput()->with('validation', $validation);
		}

		$this->cutiModel->save([
			'nama_karyawan' => $this->request->getVar('nama_karyawan'),
			'nomor_karyawan' => $this->request->getVar('nomor_karyawan'),
			'waktu_cuti' => $this->request->getVar('waktu_cuti'),
			'lama_cuti' => $this->request->getVar('lama_cuti'),
		]);

		session()->setFlashdata('pesan', 'Cuti berhasil ditambahkan');

		return redirect()->to('/history-cuti');
	}
}