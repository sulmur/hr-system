<?php namespace App\Controllers\Karyawan;


use App\Controllers\BaseController;
use App\Models\PelatihanModel;

class Pelatihan extends BaseController
{
	protected $pelatihanModel;

	public function __construct()
	{
		$this->pelatihanModel = new PelatihanModel();
	}

	public function pelatihan()
	{
		// $pelatihan = new PelatihanModel();
		// $data['pelatihan'] = $pelatihan->findAll();
		$data = [
			'title' => 'List Pelatihan | PT. Pertani',
			'pelatihan' => $this->pelatihanModel->getPelatihan()
		];
		return view("karyawan/pelatihan", $data);
	}

	public function download($id_pelatihan)
	{
		$pelatihan = new PelatihanModel();
		$dataPelatihan = $pelatihan->find($id_pelatihan);
		
		// var_dump($dataPelatihan->materi_pelatihan);
		// exit();
		
		return $this->response->download('assets/file/'.$dataPelatihan->materi_pelatihan, null);
	}
	
}