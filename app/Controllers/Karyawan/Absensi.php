<?php namespace App\Controllers\Karyawan;

use App\Controllers\BaseController;
use App\Models\AbsensiModel;

class Absensi extends BaseController
{
    protected $absensiModel;

	public function __construct()
	{
		$this->absensiModel = new AbsensiModel();
	}

    public function save()
	{
		// validasi input
		if(!$this->validate([
			'nama_karyawan' => [
                'label' => 'Nama karyawan',
				'rules' => 'required',
			],
			'nomor_karyawan' => [
                'label' => 'Nomor karyawan',
				'rules' => 'required',
            ],
            'divisi' => [
                'label' => 'Divisi',
				'rules' => 'required',
            ],
		])) {
			$validation = \Config\Services::validation();
			return redirect()->to('/absensi-lembur')->withInput()->with('validation', $validation);
		}

		$this->absensiModel->save([
			'nama_karyawan' => $this->request->getVar('nama_karyawan'),
			'nomor_karyawan' => $this->request->getVar('nomor_karyawan'),
			'divisi' => $this->request->getVar('divisi'),
            'waktu' => $this->request->getVar('waktu'),
            'tanggal' => $this->request->getVar('tanggal')
		]);

		session()->setFlashdata('pesan', 'Absen berhasil');

		return redirect()->to('/absensi-lembur');
	}
}