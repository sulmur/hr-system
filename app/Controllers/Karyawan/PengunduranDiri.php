<?php namespace App\Controllers\Karyawan;

use App\Controllers\BaseController;
use App\Models\PengunduranModel;

class PengunduranDiri extends BaseController
{
    protected $pengunduranModel;

	public function __construct()
	{
		$this->pengunduranModel = new PengunduranModel();
	}

    public function tambah_pengunduran_diri()
	{
		// session();
		$data = [
			'title' => 'Tambah Pengunduran Diri | PT. Pertani',
			'validation' => \Config\Services::validation()
		];
		return view("karyawan/tambah_pengunduran_diri", $data);
	}

    public function history_pengunduran_diri()
	{
		$data = [
			'title' => 'History Pengunduran Diri | PT. Pertani',
            'pengunduran' => $this->pengunduranModel->getPengunduran()
		];
		return view("karyawan/history_pengunduran_diri", $data);
	}

    public function save()
	{
		// validasi input
		if(!$this->validate([
			'alasan' => [
				'rules' => 'required',
				'errors' => [
					'required' => '{field} harus diisi.'
				] 
			]
		])) {
			$validation = \Config\Services::validation();
			return redirect()->to('/tambah-pengunduran-diri')->withInput()->with('validation', $validation);
		}

		$this->pengunduranModel->save([
			'nama_karyawan' => $this->request->getVar('nama_karyawan'),
			'nomor_karyawan' => $this->request->getVar('nomor_karyawan'),
			'alasan' => $this->request->getVar('alasan'),
		]);

		session()->setFlashdata('pesan', 'Data berhasil ditambahkan');

		return redirect()->to('/history-pengunduran-diri');
	}
}