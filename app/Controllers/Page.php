<?php namespace App\Controllers;

class Page extends BaseController
{
	public function login()
	{
		$data = [
			'title' => 'Login | PT. Pertani',
		];
		return view("login", $data);
	}
	
	
	public function dashboard()
	{
		$data = [
			'title' => 'Dashboard | PT. Pertani',
		];
		return view("dashboard", $data);
	}
	
}