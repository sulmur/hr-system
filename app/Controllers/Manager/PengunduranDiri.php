<?php namespace App\Controllers\Manager;

use App\Controllers\BaseController;
use App\Models\PengunduranModel;


class PengunduranDiri extends BaseController
{
	protected $pengunduranModel;

	public function __construct() 
	{
		$this->pengunduranModel = new PengunduranModel();
	}

    public function pengunduran_diri_manager()
	{
		$data = [
			'title' => 'Pengunduran diri Manager HR | PT. Pertani',
			'pengunduran' => $this->pengunduranModel->getPengunduran()
		];
		return view("manager/pengunduran_diri_manager", $data);
	}

    public function hapus($id_pengunduran_diri)
	{
		$success = $this->pengunduranModel->hapus($id_pengunduran_diri);
		if ($success) {
			session()->setFlashdata('pesan', 'Pengunduran diri telah ditolak');
			return redirect()->to('/pengunduran-diri-manager');
		}
	}
}