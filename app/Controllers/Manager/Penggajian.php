<?php namespace App\Controllers\Manager;

use App\Controllers\BaseController;
use App\Models\PenggajianModel;

class Penggajian extends BaseController
{
	protected $penggajianModel;

	public function __construct() 
	{
		$this->penggajianModel = new PenggajianModel();
		
	}

    public function penggajian_manager()
	{
		$data = [
			'title' => 'Penggajian Manager HR | PT. Pertani',
			'penggajian' => $this->penggajianModel->getPenggajian(),
		];
		return view("manager/penggajian_manager", $data);
	}

    public function hapus($id_penggajian)
	{
		$success = $this->penggajianModel->hapus($id_penggajian);
		if ($success) {
			session()->setFlashdata('pesan', 'Penggajian berhasil dihapus');
			return redirect()->to('/penggajian-manager');
		}
	}
}