<?php namespace App\Controllers\Manager;

use App\Controllers\BaseController;
use App\Models\AbsensiModel;
use App\Models\LemburModel;
use App\Models\KaryawanModel;
use App\Models\PelatihanModel;
use App\Models\CutiModel;
use App\Models\PengunduranModel;

class Manager extends BaseController
{
	protected $absensiModel;
	protected $lemburModel;
	protected $karyawanModel;
	protected $pelatihanModel;
	protected $cutiModel;
	protected $pengunduranModel;

	public function __construct() 
	{
		$this->absensiModel = new AbsensiModel();
		$this->lemburModel = new LemburModel();
		$this->karyawanModel = new KaryawanModel();
		$this->pelatihanModel = new PelatihanModel();
		$this->cutiModel = new CutiModel();
		$this->pengunduranModel = new PengunduranModel();
	}

	public function index()
	{
		$data = [
			'title' => 'Dashboard Manager HR | PT. Pertani',
			'cuti' => $this->cutiModel->getCuti(),
			'pengunduran' => $this->pengunduranModel->getPengunduran(),
			'pelatihan' => $this->pelatihanModel->getPelatihan(),
			'total_cuti' => $this->cutiModel->total_cuti(),
			'total_pengunduran' => $this->pengunduranModel->total_pengunduran(),
			'total_pelatihan' => $this->pelatihanModel->total_pelatihan()
		];
		return view("manager/index", $data);
	}

	public function list_absensi_lembur_manager()
	{
		$data = [
			'title' => 'List Absensi & Lembur Manager HR | PT. Pertani',
			'absensi' => $this->absensiModel->getAbsensi(),
			'lembur' => $this->lemburModel->getLembur()
		];
		return view("manager/list_absensi_lembur_manager", $data);
	}

	public function data_karyawan_manager()
	{
		$data = [
			'title' => 'Data Karyawan Manager HR | PT. Pertani',
			'karyawan' => $this->karyawanModel->getKaryawan()
		];
		return view("manager/data_karyawan_manager", $data);
	}

    public function laporan_manager()
	{
		$data = [
			'title' => 'Laporan Manager HR | PT. Pertani',
		];
		return view("manager/laporan_manager", $data);
	}

	public function pelatihan_manager()
	{
		$data = [
			'title' => 'List Pelatihan Manager HR | PT. Pertani',
			'pelatihan' => $this->pelatihanModel->getPelatihan()
		];
		return view("manager/pelatihan_manager", $data);
	}

    public function penerimaan_calon_karyawan()
	{
		$data = [
			'title' => 'Penerimaan Calon Karyawan | PT. Pertani',
		];
		return view("manager/penerimaan_calon_karyawan", $data);
	}

	function htmlToPDF(){
        $dompdf = new \Dompdf\Dompdf(); 
        $dompdf->loadHtml(view('pdf_view'));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();
    }
}