<?php namespace App\Controllers\Manager;

use App\Controllers\BaseController;
use App\Models\CutiModel;


class Cuti extends BaseController
{
	protected $cutiModel;

	public function __construct() 
	{
		$this->cutiModel = new CutiModel();
	}
    
    public function cuti_manager()
	{
		$data = [
			'title' => 'Cuti Manager HR | PT. Pertani',
			'cuti' => $this->cutiModel->getCuti()
		];
		return view("manager/cuti_manager", $data);
	}

    public function hapus($id_cuti)
	{
		$success = $this->cutiModel->hapus($id_cuti);
		if ($success) {
			session()->setFlashdata('pesan', 'Data cuti telah ditolak');
			return redirect()->to('/cuti-manager');
		}
	}
}