<?php namespace App\Controllers\Manager;

use App\Controllers\BaseController;
use App\Models\PenerimaanModel;


class PenerimaanKaryawan extends BaseController
{
	protected $penerimaanModel;

	public function __construct() 
	{
		$this->penerimaanModel = new PenerimaanModel();
	}

    public function penerimaan_calon_karyawan()
	{
		$data = [
			'title' => 'Penerimaan Calon Karyawan | PT. Pertani',
            'penerimaan' => $this->penerimaanModel->getPenerimaan()
		];
		return view("manager/penerimaan_calon_karyawan", $data);
	}

    public function hapus($id_penerimaan)
	{
		$success = $this->penerimaanModel->hapus($id_penerimaan);
		if ($success) {
			session()->setFlashdata('pesan', 'Data calon karyawan berhasil dihapus');
			return redirect()->to('/penerimaan-calon-karyawan');
		}
	}

	public function download($id_penerimaan)
	{
		$penerimaan = new PenerimaanModel();
		$dataPenerimaan = $penerimaan->find($id_penerimaan);
		
		// var_dump($dataPenerimaan->cv);
		// exit();
		
		return $this->response->download('assets/file/'.$dataPenerimaan->cv, null);
	}
}