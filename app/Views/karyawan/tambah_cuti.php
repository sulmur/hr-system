<?= $this->extend('layout/template_karyawan'); ?>

<?= $this->section('content'); ?>

<div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mb-3">Tambah Permohonan Cuti</h4>
                                <div class="form-validation">
                                    <form class="form-valide" action="karyawan/cuti/save" method="post">
                                        <?= csrf_field(); ?>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="nomor_karyawan">Nomor Karyawan <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="nomor_karyawan" name="nomor_karyawan" value="11180930000075" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="nama_karyawan">Nama Lengkap <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="nama_karyawan" name="nama_karyawan" value="Sulthan Muhammad Raihan" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="lama_cuti">Lama Cuti <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('lama_cuti')) ? 'is-invalid' : ''; ?>" id="lama_cuti" name="lama_cuti" placeholder="Masukkan lama cuti" autofocus value="<?= old('lama_cuti'); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('lama_cuti'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="waktu_cuti">Waktu Cuti <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('waktu_cuti')) ? 'is-invalid' : ''; ?>" id="waktu_cuti" name="waktu_cuti" placeholder="Masukkan waktu cuti" value="<?= old('waktu_cuti '); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('waktu_cuti'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-8 float-left">
                                                <a href="/history-cuti" class="btn btn-outline-form">Batal</a>
                                                <button type="submit" class="btn btn-form">Submit</button>
                                            </div>
                                        </div>                                      
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

<?= $this->endSection(); ?>