<?= $this->extend('layout/template_karyawan'); ?>

<?= $this->section('content'); ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body" style="min-height: 891px;">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-5">
                                    <h4 class="card-title mt-2 float-left" style="margin-left:30px">Permohonan Cuti</h4>
                                    <button class="btn btn-form float-right mr-4" onclick="window.print()"><i class="fa fa-print fa-lg text-white"> <span style="font-family:'Poppins'; font-size:16px"> Print</span></i></button>
                                    <a class="btn btn-login float-right mr-3" href="/tambah-cuti">Tambah Permohonan Cuti</a>
                                </div>
                                <!-- <?php if (session()->getFlashdata('pesan')) : ?>
                                    <div class="alert alert-success mx-4 mb-0" role="alert">
                                        <?= session()->getFlashdata('pesan'); ?>
                                    </div>
                                <?php endif; ?> -->
                                <div class="swal2" data-swal2="<?= session()->get('pesan'); ?>"></div>
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nomor Karyawan</th>
                                                <th>Nama Lengkap</th>
                                                <th>Lama Cuti</th>
                                                <th>Waktu Cuti</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($cuti->getResultArray() as $c) : ?>
                                            <tr>
                                                <td><?= $i++ ?></td>
                                                <td><?= $c['nomor_karyawan']; ?></td>
                                                <td><?= $c['nama_karyawan']; ?></td>
                                                <td><?= $c['lama_cuti']; ?></td>
                                                <td><?= $c['waktu_cuti']; ?></td>
                                            </tr>      
                                        <?php endforeach; ?>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


<?= $this->endSection(); ?>