<?= $this->extend('layout/template_karyawan'); ?>

<?= $this->section('content'); ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-5">
                                    <h4 class="card-title mt-2 float-left" style="margin-left:30px">Pelatihan</h4>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Pelatihan</th>
                                                <th>Tanggal</th>
                                                <th>Materi Pelatihan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($pelatihan as $p) : ?>
                                            <tr>
                                                <td><?= $i++ ?></td>
                                                <td><?= $p->nama_pelatihan; ?></td>
                                                <td><?= $p->waktu_pelatihan; ?></td>
                                                <td><?= $p->materi_pelatihan; ?></td>
                                                <td>
                                                    <a class="btn btn-form btn-sm" href="/karyawan/pelatihan/download/<?= $p->id_pelatihan; ?>"><i class="fa fa-print fa-lg"></i></a>
                                                </td>
                                            </tr>                                                              
                                        <?php endforeach; ?>                 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

<?= $this->endSection(); ?>