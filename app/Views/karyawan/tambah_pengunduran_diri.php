<?= $this->extend('layout/template_karyawan'); ?>

<?= $this->section('content'); ?>

<div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mb-3">Tambah Pengunduran Diri</h4>
                                <div class="form-validation">
                                    <form class="form-valide" action="karyawan/pengundurandiri/save" method="post">
                                        <?= csrf_field(); ?>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="nomor_karyawan">Nomor Karyawan <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="nomor_karyawan" name="nomor_karyawan" value="11180930000076" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="nama_karyawan">Nama Lengkap <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="nama_karyawan" name="nama_karyawan" value="Muhammad Faiz Kurniawan" readonly="readonly">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="alasan">Alasan <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('alasan')) ? 'is-invalid' : ''; ?>" id="alasan" name="alasan" placeholder="Masukkan alasan" autofocus value="<?= old('alasan'); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('alasan'); ?>
                                                </div>
                                            </div>
                                        </div>    
                                        <div class="form-group row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-8 float-left">
                                                <a href="/history-pengunduran-diri" class="btn btn-outline-form">Batal</a>
                                                <button type="submit" class="btn btn-form">Submit</button>
                                            </div>
                                        </div>                                   
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

<?= $this->endSection(); ?>