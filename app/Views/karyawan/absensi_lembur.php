<?= $this->extend('layout/template_karyawan'); ?>

<?= $this->section('content'); ?>

<div class="content-body">

    <div class="row page-titles mx-0">
        <!-- <div class="col p-md-0">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
            </ol>
        </div> -->
    </div>
    <div class="swal2" data-swal2="<?= session()->get('pesan'); ?>"></div>
    <div class="login-form-bg h-100 my-5 offset-0">
        <ul class="nav nav-pills ml-5 mb-5" id="pills-Tab" role="tablist">
            <li class="nav-item mr-2" role="presentation">
                <a class="btn btn-contact active" id="pills-absen-tab" data-toggle="pill" href="#pills-absen" role="tab" aria-controls="pills-absen" aria-selected="true">Absensi</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="btn btn-contact" id="pills-lembur-tab" data-toggle="pill" href="#pills-lembur" role="tab" aria-controls="pills-lembur" aria-selected="false">Lembur</a>
            </li>
        </ul>
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content">                                                

                            <div class="card login-form mb-0">
                                <div class="tab-content" id="pills-tabContent">
                                    
                                    <div class="tab-pane fade show active" id="pills-absen" role="tabpanel" aria-labelledby="pills-absen-tab">
                                        <div class="card-body pt-5">
                                            <div class="text-center">
                                                <h3>Absen Karyawan PT. Pertani</h3>
                                            </div>
                                            <!-- <?php if  (session()->get('pesan')) : ?>
                                                <div class="alert alert-success mb-0 alert-dismissible fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                        <strong><?= session()->getFlashdata('pesan'); ?></strong>
                                                </div>
                                            <?php endif; ?> -->
                                            <form class="mt-5 mb-2 login-input" action="/karyawan/absensi/save" method="post">
                                                <?= csrf_field(); ?>
                                                <div class="form-group">
                                                    <input type="text" class="form-control <?= ($validation->hasError('nomor_karyawan')) ? 'is-invalid' : ''; ?>" id="nama_karyawan" name="nama_karyawan" placeholder="Nomor Karyawan" value="<?= old('nomor_karyawan'); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError('nomor_karyawan'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control <?= ($validation->hasError('nama_karyawan')) ? 'is-invalid' : ''; ?>" id="nomor_karyawan" name="nomor_karyawan" placeholder="Nama Lengkap" value="<?= old('nama_karyawan'); ?>">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError('nama_karyawan'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control <?= ($validation->hasError('divisi')) ? 'is-invalid' : ''; ?>" id="divisi" name="divisi" value="<?= old('divisi'); ?>">
                                                        <option value="">Pilih Divisi</option>
                                                        <option value="HR">HR</option>
                                                        <option value="Finance">Finance</option>
                                                        <option value="Sales & Marketing">Sales & Marketing</option>
                                                        <option value="Produksi">Produksi</option>
                                                        <option value="Purchasing">Purchasing</option>
                                                        <option value="Inventory">Inventory</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError('divisi'); ?>
                                                    </div>
                                                </div>
                                                <div class="text-center">
                                                    <button type="submit" class="btn btn-login submit font-weight-bold rounded-pill w-25">ABSEN</button>
                                                </div>
                                            </form>                                    
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="pills-lembur" role="tabpanel" aria-labelledby="pills-lembur-tab">
                                        <div class="card-body pt-5">
                                            <div class="text-center">
                                                <h3>Lembur Karyawan PT. Pertani</h3>
                                            </div>
                                            <!-- <?php if  (session()->get('pesan')) : ?>
                                                <div class="alert alert-success mb-3 alert-dismissible fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                        <strong><?= session()->getFlashdata('pesan'); ?></strong>
                                                </div>
                                            <?php endif; ?> -->
                                            <form class="mt-5 mb-2 login-input" action="/karyawan/lembur/save" method="post">
                                                <?= csrf_field(); ?>
                                                <div class="form-group">
                                                    <input type="text" class="form-control <?= ($validation->hasError('nomor_karyawan')) ? 'is-invalid' : ''; ?>" id="nomor_karyawan" name="nomor_karyawan" placeholder="Nomor Karyawan" autofocus>
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError('nomor_karyawan'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control <?= ($validation->hasError('nama_karyawan')) ? 'is-invalid' : ''; ?>" id="nama_karyawan" name="nama_karyawan" placeholder="Nama Lengkap">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError('nama_karyawan'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control <?= ($validation->hasError('divisi')) ? 'is-invalid' : ''; ?>" id="divisi" name="divisi" value="<?= old('divisi'); ?>">
                                                        <option value="">Pilih Divisi</option>
                                                        <option value="HR">HR</option>
                                                        <option value="Finance">Finance</option>
                                                        <option value="Sales & Marketing">Sales & Marketing</option>
                                                        <option value="Produksi">Produksi</option>
                                                        <option value="Purchasing">Purchasing</option>
                                                        <option value="Inventory">Inventory</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError('divisi'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control <?= ($validation->hasError('tanggal_lembur')) ? 'is-invalid' : ''; ?>" id="tanggal_lembur" name="tanggal_lembur" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Tanggal Lembur">
                                                    <div class="invalid-feedback">
                                                        <?= $validation->getError('tanggal_lembur'); ?>
                                                    </div>
                                                </div>
                                                <div class="text-center">
                                                    <button type="submit" class="btn btn-login submit font-weight-bold rounded-pill w-25">SUBMIT</button>
                                                </div>
                                            </form>                                    
                                        </div>
                                    </div>
                                
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>