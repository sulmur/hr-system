<?= $this->extend('layout/template_admin'); ?>

<?= $this->section('content'); ?>

<div class="content-body" style="min-height: 891px;">

    <div class="row page-titles mx-0">
        <div class="col p-md-0">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
            </ol>
        </div>
    </div>
    <!-- row -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="mb-5" style="margin-left:30px">
                            <h2 class="mt-2">Total Pegawai</h2>
                        </div>
                    
                        <div class="row mx-3">
                            <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-1">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">HRD</h3>
                                        <div class="d-inline-block">
                                            <h4 class="text-white">#</h4>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-users"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-2">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">Finance</h3>
                                        <div class="d-inline-block">
                                            <h4 class="text-white">#</h4>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-money"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-3">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">Production</h3>
                                        <div class="d-inline-block">
                                            <h4 class="text-white">#</h4>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-industry"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mx-3">
                            <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-5">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">Purchasing</h3>
                                        <div class="d-inline-block">
                                            <h4 class="text-white">#</h4>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-credit-card"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-7">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">Sales & Marketing</h3>
                                        <div class="d-inline-block">
                                            <h4 class="text-white">#</h4>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-shopping-cart"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-6">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">Inventory</h3>
                                        <div class="d-inline-block">
                                            <h4 class="text-white">#</h4>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-truck text-white"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                            <!-- <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-4">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">Customer Satisfaction</h3>
                                        <div class="d-inline-block">
                                            <h2 class="text-white">99%</h2>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-heart"></i></span>
                                    </div>
                                </div>
                            </div> -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->

<?= $this->endSection(); ?>