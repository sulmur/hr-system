<?= $this->extend('layout/template_home'); ?>

<?= $this->section('content'); ?>

<section id="join">
    <div class="container">
        <div class="row pb-5" style="padding-top:10rem">
            <div class="col-md-6 mt-5">
                <h1 class="text-white poppins font-weight-bold mt-5 mb-5">Gabung Bersama Kami!</h1>
                <h5 class="text-white poppins mb-5" style="max-width:82%">Buat masa depan anda semakin cerah bersama kami untuk membangun Indonesia lebih baik lagi!</h5>
                <a class="btn btn-join font-weight-bold rounded-pill p-3" data-scroll href="#form">Gabung Sekarang</a>
            </div>
            <div class="col-md-6">
                <img draggable="false" src="assets/images/bg/hero-join.svg" alt="">
            </div>
        </div>
    </div>
    <img draggable="false" src="assets/images/bg/wave.svg" class="w-100" alt="">
</section>

<section id="alur">
    <div class="container">
        <div class="text-center mb-5">
            <h3 class="text-green font-weight-bold poppins">Alur Pendaftaran</h3>
            <div class="line"></div>
            <img draggable="false" src="assets/images/bg/alur-pendaftaran.svg" class="w-100 mt-5" alt="">
        </div>
    </div>
</section>

<section id="form">
    <div class="container pt-5">
    <!-- <button onclick="Swal.fire()">Test</button> -->
        <div class="text-center mb-5">
            <h3 class="text-white font-weight-bold poppins mb-3">Formulir Pendaftaran</h3>
            <div class="white-line mx-auto mb-5"></div>
            
            <div class="swal" data-swal="<?= session()->get('pesan'); ?>"></div>

            <form action="home/save" class="mx-auto w-75" method="post" enctype="multipart/form-data">
                <?= csrf_field(); ?>
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 col-form-label text-white text-right" style="padding-top:12px">Nama</label>
                    <input type="text" class="form-control col-sm-10 <?= ($validation->hasError('nama')) ? 'is-invalid' : ''; ?>" id="nama" name="nama" placeholder="Nama lengkap">
                </div>
                <div class="form-group row">
                    <label for="tanggal_lahir" class="col-sm-2 col-form-label text-white text-right" style="padding-top:12px">Tanggal Lahir</label>
                    <input type="date" class="form-control col-sm-10 <?= ($validation->hasError('tanggal_lahir')) ? 'is-invalid' : ''; ?>" id="tanggal_lahir" name="tanggal_lahir" placeholder="Tanggal Lahir">
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label text-white text-right" style="padding-top:12px">Email</label>
                    <input type="text" class="form-control col-sm-10 <?= ($validation->hasError('email')) ? 'is-invalid' : ''; ?>" id="email" name="email" placeholder="Email">
                </div>
                <div class="form-group row">
                    <label for="divisi" class="col-sm-2 col-form-label text-white text-right" style="padding-top:12px">Divisi</label>
                    <select class="form-control col-sm-10 <?= ($validation->hasError('divisi')) ? 'is-invalid' : ''; ?>" id="divisi" name="divisi">
                        <option value="">Divisi yang dilamar</option>
                        <option value="HR">HR</option>
                        <option value="Finance">Finance</option>
                        <option value="Sales & Marketing">Sales & Marketing</option>
                        <option value="Produksi">Produksi</option>
                        <option value="Purchasing">Purchasing</option>
                        <option value="Inventory">Inventory</option>
                    </select>
                </div>
                <div class="form-group row">
                    <label for="no_telepon" class="col-sm-2 col-form-label text-white text-right" style="padding-top:12px">Nomor Telepon</label>
                    <input type="text" class="form-control col-sm-10 <?= ($validation->hasError('no_telepon')) ? 'is-invalid' : ''; ?>" id="no_telepon" name="no_telepon" placeholder="Nomor Telepon">
                </div>
                <div class="form-group row">
                    <label for="pengalaman" class="col-sm-2 col-form-label text-white text-right" style="padding-top:12px">Pengalaman</label>
                    <input type="text" class="form-control col-sm-10 <?= ($validation->hasError('pengalaman')) ? 'is-invalid' : ''; ?>" id="pengalaman" name="pengalaman" placeholder="Pengalaman">
                </div>
                <div class="form-group row">
                <label for="alamat" class="col-sm-2 col-form-label text-white text-right" style="padding-top:12px">Alamat</label>
                    <input type="text" class="form-control col-sm-10 <?= ($validation->hasError('alamat')) ? 'is-invalid' : ''; ?>" id="alamat" name="alamat" placeholder="Alamat">
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label text-white text-right" style="padding-top:8px" for="cv">CV</label>
                    <div class="custom-file col-sm-10 text-justify">
                        <input type="file" class="custom-file-input <?= ($validation->hasError('cv')) ? 'is-invalid' : ''; ?>" id="cv" name="cv" onchange="previewFile()">
                        <label class="custom-file-label" for="cv">Masukkan CV</label>
                    </div>
                </div>
                <div class="mt-4">
                    <!-- <button type="submit" class="btn btn-register font-weight-bold rounded-pill py-3 px-5" data-toggle="modal" data-target="#berhasil">Daftar</button> -->
                    <button type="submit" class="btn btn-register font-weight-bold rounded-pill py-3 px-5">Daftar</button>
                    <!-- <div class="modal fade" id="berhasil">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header text-center">
                                    <h5 class="modal-title">Pendaftaran Berhasil</h5>
                                </div>
                                <div class="modal-body">
                                    <i class="fa fa-check fa-3x"></i>
                                </div>
                                <div class="modal-footer text-center">
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </form>
        </div>
    </div>
</section>

<section id="contact">
    <div class="container pt-5">
    <div class="text-center mb-5">
        <h3 class="text-green font-weight-bold poppins">Kontak Kami</h3>
        <div class="line"></div>
    </div>
    <div class="row pb-5 mt-5">
        <div class="col-md-6">
            <img draggable="false" src="assets/images/bg/hero-contact.svg" alt="">
        </div>
        <div class="col-md-6">
            <h4 class="poppins">ALAMAT</h4>
            <p class="text-justify poppins mb-3">Gedung Graha Gabah No.1, RT.6/RW.3, Jalan Raya Pasar Minggu, Duren Tiga, Pancoran, RT.6/RW.3, Duren Tiga, Kec. Pancoran, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12760</p>
            <h4 class="poppins">TELEPON</h4>
            <p class="poppins mb-3"><img draggable="false" src="assets/images/bg/phone.svg" class="mr-3" alt="">(021) 7993108</p>
            <h4 class="poppins">LAYANAN ONLINE</h4>
            <p class="poppins"><img draggable="false" src="assets/images/bg/web.svg" class="mr-3" alt="">www.pertani.co.id</p>
            <p class="poppins"><img draggable="false" src="assets/images/bg/email.svg" class="mr-3" alt="">pusat@pertani.co.id</p>
        </div>
    </div>
    </div>
</section>

<?= $this->endSection(); ?>