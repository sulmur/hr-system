<?= $this->extend('layout/template_admin'); ?>

<?= $this->section('content'); ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0"></div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                    <h4 class="card-title mt-2 float-left" style="margin-left:30px">Permohonan Cuti</h4>
                                    <button class="btn btn-form float-right mr-4" onclick="window.print()"><i class="fa fa-print fa-lg text-white"> <span style="font-family:'Poppins'; font-size:16px"> Laporan</span></i></button>
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nomor Karyawan</th>
                                                <th>Nama Lengkap</th>
                                                <th>Lama Cuti</th>
                                                <th>Waktu Cuti</th>
                                                <!-- <th>Aksi</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($cuti->getResultArray() as $c) : ?>
                                            <tr>
                                                <td><?= $i++ ?></td>
                                                <td><?= $c['nomor_karyawan']; ?></td>
                                                <td><?= $c['nama_karyawan']; ?></td>
                                                <td><?= $c['lama_cuti']; ?></td>
                                                <td><?= $c['waktu_cuti']; ?></td>
                                                <!-- <td><button type="button" style="border: 0px transparent"><i class="fa fa-print fa-2x" style="color:#4F8937"></i></button></td> -->
                                            </tr>      
                                            <?php endforeach; ?>                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

<?= $this->endSection(); ?>