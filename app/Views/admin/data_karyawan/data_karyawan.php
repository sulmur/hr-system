<?= $this->extend('layout/template_admin'); ?>

<?= $this->section('content'); ?>

<style>
    @media print {
        @page {

        }
        th:nth-child(5),
        td:nth-child(5),
        #debug-icon-link {
            display: none;
        }
    }
</style>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-5">
                                    <h4 class="card-title mt-2 float-left" style="margin-left:30px">Data Karyawan</h4>
                                    <button class="btn btn-form float-right mr-4" onclick="window.print()"><i class="fa fa-print fa-lg text-white"> <span style="font-family:'Poppins'; font-size:16px"> Laporan</span></i></button>
                                    <button type="button" class="btn btn-login float-right mr-3" data-toggle="modal" data-target="#tambah-karyawan">Tambah Data Karyawan</button>
                                    <!-- <a href="/tambah-data-karyawan" class="btn btn-login float-right mr-4">Tambah Data Karyawan</a> -->
                                </div>
<!-- 
                                <?php if  (session()->get('pesan')) : ?>
                                    <div class="alert alert-success mx-4 mb-0 alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                            <strong><?= session()->getFlashdata('pesan'); ?></strong>
                                    </div>
                                <?php endif; ?> -->

                                <div class="swal2" data-swal2="<?= session()->get('pesan'); ?>"></div>

                                <div class="row mb-0">
                                    <div class="col-md-6">
                                        <?php 
                                            if(session()->get('error')) {
                                                echo "<div class='alert alert-danger pt-2 pb-0 mb-0' role='alert'>". session()->get('error') ."</div>";
                                                session()->remove('error');
                                            }
                                        ?>
                                    </div>
                                </div>

                                <!-- Modal tambah karyawan -->
                                
                                <div class="modal fade" id="tambah-karyawan">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">   
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title mb-3">Tambah Data Karyawan</h4>
                                                    <div class="form-validation">
                                                        <form class="form-valide" action="/admin/datakaryawan/tambah" method="post">
                                                            <?= csrf_field(); ?>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label" for="nomor_karyawan">Nomor Karyawan <span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control" id="nomor_karyawan" name="nomor_karyawan" autofocus value="<?= old('nomor_karyawan'); ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label" for="nama_karyawan">Nama Lengkap <span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control" id="nama_karyawan" name="nama_karyawan" placeholder="Masukkan nama lengkap" value="<?= old('nama_karyawan'); ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label" for="divisi">Divisi <span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-lg-6">
                                                                    <!-- <input type="text" class="form-control" id="divisi" name="divisi" placeholder="Pilih divisi"> -->
                                                                    <select class="form-control" id="divisi" name="divisi" value="<?= old('divisi'); ?>">
                                                                        <option value="">Pilih Divisi</option>
                                                                        <option value="HR">HR</option>
                                                                        <option value="Finance">Finance</option>
                                                                        <option value="Sales & Marketing">Sales & Marketing</option>
                                                                        <option value="Produksi">Produksi</option>
                                                                        <option value="Purchasing">Purchasing</option>
                                                                        <option value="Inventory">Inventory</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label" for="jabatan">Jabatan <span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Masukkan Jabatan" value="<?= old('jabatan'); ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label" for="tanggal_masuk">Tanggal Masuk<span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control" id="tanggal_masuk" name="tanggal_masuk" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Masukkan tanggal masuk" value="<?= old('tanggal_masuk'); ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label" for="tanggal_lahir">Tanggal Lahir<span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control" id="tanggal_lahir" name="tanggal_lahir" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Masukkan tanggal lahir" value="<?= old('tanggal_lahir'); ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label" for="jenis_kelamin">Jenis Kelamin <span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-lg-6">                                                
                                                                    <select class="form-control" id="jenis_kelamin" name="jenis_kelamin" value="<?= old('jenis_kelamin'); ?>">
                                                                        <option value="">Jenis Kelamin</option>
                                                                        <option value="Laki-laki">Laki-laki</option>
                                                                        <option value="Perempuan">Perempuan</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label" for="agama">Agama <span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control" id="agama" name="agama" placeholder="Masukkan Agama" value="<?= old('agama'); ?>">
                                                                </div>
                                                            </div>                                              
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label" for="alamat">Alamat <span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-lg-6">
                                                                    <textarea class="form-control" id="alamat" name="alamat" rows="5" placeholder="Masukkan alamat karyawan" value="<?= old('alamat'); ?>"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-lg-4"></div>
                                                                <div class="col-lg-8 float-left">
                                                                    <button type="button" class="btn btn-outline-form" data-dismiss="modal">Batal</button>
                                                                    <button type="submit" class="btn btn-form">Submit</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-hover table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Karyawan</th>
                                                <th>Nomor Karyawan</th>
                                                <th>Divisi</th>
                                                <!-- <th>Status</th> -->
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($karyawan->getResultArray() as $k) : ?>
                                            <tr>
                                                <td><?= $i++ ?></td>
                                                <td><?= $k['nama_karyawan']; ?></td>
                                                <td><?= $k['nomor_karyawan']; ?></td>
                                                <td><?= $k['divisi']; ?></td>
                                                <!-- <td>Aktif</td> -->
                                                <td>
                                                    <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#detail-data-karyawan" id="btn-info" data-id="<?= $k['nomor_karyawan']; ?>" data-nomor="<?= $k['nomor_karyawan']; ?>" data-nama="<?= $k['nama_karyawan']; ?>" data-divisi="<?= $k['divisi']; ?>" data-jabatan="<?= $k['jabatan']; ?>" data-tanggal-masuk="<?= $k['tanggal_masuk']; ?>" data-tanggal-lahir="<?= $k['tanggal_lahir']; ?>" data-agama="<?= $k['agama']; ?>" data-alamat="<?= $k['alamat']; ?>"><i class="fa fa-info-circle fa-lg"></i></button>
                                                    <button type="button" class="btn btn-sm btn-warning text-white" data-toggle="modal" data-target="#edit-karyawan" id="btn-edit" data-id="<?= $k['id_karyawan']; ?>" data-nomor="<?= $k['nomor_karyawan']; ?>" data-nama="<?= $k['nama_karyawan']; ?>" data-divisi="<?= $k['divisi']; ?>" data-jabatan="<?= $k['jabatan']; ?>" data-tanggal-masuk="<?= $k['tanggal_masuk']; ?>" data-tanggal-lahir="<?= $k['tanggal_lahir']; ?>" data-agama="<?= $k['agama']; ?>" data-alamat="<?= $k['alamat']; ?>"><i class="fa fa-edit fa-lg"></i></button>
                                                    <form action="/admin/datakaryawan/hapus/<?= $k['id_karyawan']; ?>" style="display:contents" method="post">
                                                        <?= csrf_field(); ?>
                                                        <input type="hidden" name="_method" value="DELETE">    
                                                        <button type="submit" class="btn btn-sm btn-danger" id="<?= $k['id_karyawan']; ?>"><i class="fa fa-trash fa-lg"></i></button>
                                                    </form> 
                                                    <!-- <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#hapus-karyawan"><i class="fa fa-trash fa-lg"></i></button> -->
                                                </td>
                                            </tr>   

                                            <!-- Modal Hapus -->
                                            <!-- <div class="modal fade" id="hapus-karyawan">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header text-center">
                                                            <h5 class="modal-title">Hapus Karyawan</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4>Apakah anda yakin menghapus <?= $k['nama_karyawan'];?>?</h4>
                                                        </div>
                                                        <div class="modal-footer text-center">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>

                                                            <form action="/admin/datakaryawan/hapus/<?= $k['id_karyawan']; ?>" method="post">
                                                                <?= csrf_field(); ?>
                                                                <input type="hidden" name="_method" value="DELETE">    
                                                                <button type="submit" class="btn btn-success text-white" id="<?= $k['id_karyawan']; ?>">Ya</button>
                                                            </form> 

                                                            <a href="/admin/datakaryawan/delete/" class="btn btn-success text-white">Ya</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                                                   -->
                                                                                
                                        <?php endforeach; ?>                 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->             
        
        
        <!-- Modal Detail -->
        <div class="modal fade" id="detail-data-karyawan">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Detail Karyawan</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6">
                                <b>Nomor Pegawai</b>
                            </div>
                            <div class="col-6">
                                <p id="nomor_karyawan">: <?= $k['nomor_karyawan']; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <b>Nama</b>
                            </div>
                            <div class="col-6">
                                <p>: <?= $k['nama_karyawan']; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <b>Divisi</b>
                            </div>
                            <div class="col-6">
                                <p>: <?= $k['divisi']; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <b>Jabatan</b>
                            </div>
                            <div class="col-6">
                                <p>: <?= $k['jabatan']; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <b>Tanggal Masuk</b>
                            </div>
                            <div class="col-6">
                                <p>: <?= $k['tanggal_masuk']; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <b>Tanggal Lahir</b>
                            </div>
                            <div class="col-6">
                                <p>: <?= $k['tanggal_lahir']; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <b>Agama</b>
                            </div>
                            <div class="col-6">
                                <p>: <?= $k['agama']; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <b>Alamat</b>
                            </div>
                            <div class="col-6">
                                <p>: <?= $k['alamat']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        
        <!-- Modal Edit  -->
        <div class="modal fade" id="edit-karyawan">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Karyawan</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="admin/datakaryawan/ubah" method="post">
                            <input type="hidden" class="form-control" id="id_karyawan" name="id_karyawan" value="<?= $k['id_karyawan']; ?>">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="nomor_karyawan">Nomor Karyawan <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control <?= ($validation->hasError('nomor_karyawan')) ? 'is-invalid' : ''; ?>" id="nomor_karyawan" name="nomor_karyawan" placeholder="Masukkan nomor karyawan" autofocus value="<?= old('nomor_karyawan'); ?>">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError('nomor_karyawan'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="nama_karyawan">Nama Lengkap <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control <?= ($validation->hasError('nama_karyawan')) ? 'is-invalid' : ''; ?>" id="nama_karyawan" name="nama_karyawan" placeholder="Masukkan nama lengkap" value="<?= old('nama_karyawan'); ?>">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError('nama_karyawan'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="divisi">Divisi <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <!-- <input type="text" class="form-control" id="divisi" name="divisi" placeholder="Pilih divisi"> -->
                                    <select class="form-control <?= ($validation->hasError('divisi')) ? 'is-invalid' : ''; ?>" id="divisi" name="divisi" value="<?= old('divisi'); ?>">
                                        <option value="">Pilih Divisi</option>
                                        <option value="HR">HR</option>
                                        <option value="Finance">Finance</option>
                                        <option value="Sales & Marketing">Sales & Marketing</option>
                                        <option value="Produksi">Produksi</option>
                                        <option value="Purchasing">Purchasing</option>
                                        <option value="Inventory">Inventory</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        <?= $validation->getError('divisi'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="jabatan">Jabatan <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control <?= ($validation->hasError('jabatan')) ? 'is-invalid' : ''; ?>" id="jabatan" name="jabatan" placeholder="Masukkan Jabatan" value="<?= old('jabatan'); ?>">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError('jabatan'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="tanggal_masuk">Tanggal Masuk<span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <input type="date" class="form-control <?= ($validation->hasError('tanggal_masuk')) ? 'is-invalid' : ''; ?>" id="tanggal_masuk" name="tanggal_masuk" placeholder="12-03-2021" value="<?= old('tanggal_masuk'); ?>">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError('tanggal_masuk'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="tanggal_lahir">Tanggal Lahir<span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <input type="date" class="form-control <?= ($validation->hasError('tanggal_lahir')) ? 'is-invalid' : ''; ?>" id="tanggal_lahir" name="tanggal_lahir" placeholder="12-03-2021" value="<?= old('tanggal_lahir'); ?>">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError('tanggal_lahir'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="jenis_kelamin">Jenis Kelamin <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">                                                
                                    <select class="form-control <?= ($validation->hasError('jenis_kelamin')) ? 'is-invalid' : ''; ?>" id="jenis_kelamin" name="jenis_kelamin" value="<?= old('jenis_kelamin'); ?>">
                                        <option value="">Jenis Kelamin</option>
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        <?= $validation->getError('jenis_kelamin'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="agama">Agama <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control <?= ($validation->hasError('agama')) ? 'is-invalid' : ''; ?>" id="agama" name="agama" placeholder="Masukkan Agama" value="<?= old('agama'); ?>">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError('agama'); ?>
                                    </div>
                                </div>
                            </div>                                              
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="alamat">Alamat <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-6">
                                    <textarea class="form-control <?= ($validation->hasError('alamat')) ? 'is-invalid' : ''; ?>" id="alamat" name="alamat" rows="5" placeholder="Masukkan alamat karyawan" value="<?= old('alamat'); ?>"></textarea>
                                    <div class="invalid-feedback">
                                        <?= $validation->getError('alamat'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mt-3">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-8 float-left">
                                    <button type="button" class="btn btn-outline-form" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-form">Ubah</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- <div class="modal-footer">
                        
                    </div> -->
                </div>
            </div>
        </div>   

<?= $this->endSection(); ?>