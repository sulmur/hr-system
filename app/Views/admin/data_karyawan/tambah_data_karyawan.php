<?= $this->extend('layout/template_admin'); ?>

<?= $this->section('content'); ?>

<div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mb-3">Tambah Data Karyawan</h4>
                                <div class="form-validation">
                                    <form class="form-valide" action="admin/datakaryawan/save" method="post">
                                        <?= csrf_field(); ?>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="nomor_karyawan">Nomor Karyawan <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('nomor_karyawan')) ? 'is-invalid' : ''; ?>" id="nomor_karyawan" name="nomor_karyawan" placeholder="Masukkan nomor karyawan" autofocus value="<?= old('nomor_karyawan'); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('nomor_karyawan'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="nama_karyawan">Nama Lengkap <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('nama_karyawan')) ? 'is-invalid' : ''; ?>" id="nama_karyawan" name="nama_karyawan" placeholder="Masukkan nama lengkap" value="<?= old('nama_karyawan'); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('nama_karyawan'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="divisi">Divisi <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <!-- <input type="text" class="form-control" id="divisi" name="divisi" placeholder="Pilih divisi"> -->
                                                <select class="form-control <?= ($validation->hasError('divisi')) ? 'is-invalid' : ''; ?>" id="divisi" name="divisi" value="<?= old('divisi'); ?>">
                                                    <option value="">Pilih Divisi</option>
                                                    <option value="HR">HR</option>
                                                    <option value="Finance">Finance</option>
                                                    <option value="Sales & Marketing">Sales & Marketing</option>
                                                    <option value="Produksi">Produksi</option>
                                                    <option value="Purchasing">Purchasing</option>
                                                    <option value="Inventory">Inventory</option>
                                                </select>
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('divisi'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="jabatan">Jabatan <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('jabatan')) ? 'is-invalid' : ''; ?>" id="jabatan" name="jabatan" placeholder="Masukkan Jabatan" value="<?= old('jabatan'); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('jabatan'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="tanggal_masuk">Tanggal Masuk<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="date" class="form-control <?= ($validation->hasError('tanggal_masuk')) ? 'is-invalid' : ''; ?>" id="tanggal_masuk" name="tanggal_masuk" placeholder="12-03-2021" value="<?= old('tanggal_masuk'); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('tanggal_masuk'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="tanggal_lahir">Tanggal Lahir<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="date" class="form-control <?= ($validation->hasError('tanggal_lahir')) ? 'is-invalid' : ''; ?>" id="tanggal_lahir" name="tanggal_lahir" placeholder="12-03-2021" value="<?= old('tanggal_lahir'); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('tanggal_lahir'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="jenis_kelamin">Jenis Kelamin <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">                                                
                                                <select class="form-control <?= ($validation->hasError('jenis_kelamin')) ? 'is-invalid' : ''; ?>" id="jenis_kelamin" name="jenis_kelamin" value="<?= old('jenis_kelamin'); ?>">
                                                    <option value="">Jenis Kelamin</option>
                                                    <option value="Laki-laki">Laki-laki</option>
                                                    <option value="Perempuan">Perempuan</option>
                                                </select>
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('jenis_kelamin'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="agama">Agama <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('agama')) ? 'is-invalid' : ''; ?>" id="agama" name="agama" placeholder="Masukkan Agama" value="<?= old('agama'); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('agama'); ?>
                                                </div>
                                            </div>
                                        </div>                                              
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="alamat">Alamat <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <textarea class="form-control <?= ($validation->hasError('alamat')) ? 'is-invalid' : ''; ?>" id="alamat" name="alamat" rows="5" placeholder="Masukkan alamat karyawan" value="<?= old('alamat'); ?>"></textarea>
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('alamat'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-8 float-left">
                                                <a href="/data-karyawan" class="btn btn-outline-form">Batal</a>
                                                <button type="submit" class="btn btn-form">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

<?= $this->endSection(); ?>