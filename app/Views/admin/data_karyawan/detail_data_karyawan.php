<?= $this->extend('layout/template_admin'); ?>

<?= $this->section('content'); ?>

    <div class="content-body">

        <div class="row page-titles mx-0">
            <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mt-2 float-left" style="margin-left:30px">Detail Karyawan</h4>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <td>Nomor Karyawan</td>
                                        <td>:<?= $karyawan['nomor_karyawan']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Karyawan</td>
                                        <td>:<?= $karyawan['nama_karyawan']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Divisi</td>
                                        <td>:<?= $karyawan['divisi']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jabatan</td>
                                        <td>:<?= $karyawan['jabatan']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Masuk</td>
                                        <td>:<?= $karyawan['tanggal_masuk'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>:<?= $karyawan['tanggal_lahir'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>:<?= $karyawan['jenis_kelamin'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Agama</td>
                                        <td>:<?= $karyawan['agama'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:<?= $karyawan['alamat'];?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?= $this->endSection(); ?>