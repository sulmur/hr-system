<?= $this->extend('layout/template_admin'); ?>

<?= $this->section('content'); ?>

<style>
    @media print {
        @page {
            margin-right: 0px
        }
        th:nth-child(5),
        td:nth-child(5) {
            display: none;
        }
    }
</style>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body"> 
                                    <h4 class="card-title mt-2 float-left" style="margin-left:30px">Data Calon Karyawan</h4>
                                    <button class="btn btn-form float-right mr-4" onclick="window.print()"><i class="fa fa-print fa-lg text-white"> <span style="font-family:'Poppins'; font-size:16px"> Laporan</span></i></button>
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Peserta</th>
                                                <th>Posisi</th>
                                                <th>Berkas</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($penerimaan->getResultArray() as $pe) : ?>
                                            <tr>
                                                <td><?= $i++ ?></td>
                                                <td><?= $pe['nama']; ?></td>
                                                <td><?= $pe['divisi']; ?></td>
                                                <td><?= $pe['cv']; ?></td>
                                                <!-- <td><i class="fa fa-file fa-lg" style="color:#4F8937"></i></td> -->
                                                <td><button type="button" class="border-0 bg-transparent" data-toggle="modal" data-target="#detail-calon-karyawan"><i class="fa fa-info-circle fa-lg" style="color:#4F8937"></i></button></td>
                                            </tr>   
                                            <div class="modal fade" id="detail-calon-karyawan">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Detail Karyawan</h5>
                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Nomor Peserta</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $pe['id_penerimaan']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Nama</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $pe['nama']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Divisi</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $pe['divisi']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Email</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $pe['email']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Nomor Telepon</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $pe['no_telepon']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Tanggal Lahir</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $pe['tanggal_lahir']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Pengalaman</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $pe['pengalaman']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Alamat</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $pe['alamat']; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save changes</button> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>        
                                        <?php endforeach; ?>                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

<?= $this->endSection(); ?>