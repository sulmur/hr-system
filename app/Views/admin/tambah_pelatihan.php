<?= $this->extend('layout/template_admin'); ?>

<?= $this->section('content'); ?>

<style>
    @media print {
        @page {
            margin-right: 0px
        }
        th:nth-child(3),
        td:nth-child(3) {
            display: none;
        }
    }
</style>

<div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mb-3">Tambah Pelatihan</h4>
                                <div class="swal2" data-swal2="<?= session()->get('pesan'); ?>"></div>
                                <div class="form-validation">
                                    <form class="form-valide" action="admin/tambahpelatihan/save" method="post" enctype="multipart/form-data">
                                        <?= csrf_field(); ?>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="nama_pelatihan">Nama Pelatihan <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('nama_pelatihan')) ? 'is-invalid' : ''; ?>" id="nama_pelatihan" name="nama_pelatihan" placeholder="Masukkan nama pelatihan" autofocus value="<?= old('nama_pelatihan'); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('nama_pelatihan'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="waktu_pelatihan">Tanggal <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('waktu_pelatihan')) ? 'is-invalid' : ''; ?>" id="waktu_pelatihan" name="waktu_pelatihan" placeholder="Masukkan tanggal pelatihan" value="<?= old('waktu_pelatihan'); ?>">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('waktu_pelatihan'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="materi_pelatihan">Materi<span class="text-danger">*</span>
                                            </label>
                                            <div class="custom-file col-lg-6 text-justify" style="margin-left:15px">
                                                <input type="file" class="custom-file-input <?= ($validation->hasError('materi_pelatihan')) ? 'is-invalid' : ''; ?>" id="materi_pelatihan" name="materi_pelatihan" onchange="previewFile()">
                                                <label class="custom-file-label" for="materi_pelatihan">Masukkan Materi</label>
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('materi_pelatihan'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-8 float-left">
                                                <a href="/admin" class="btn btn-outline-form">Batal</a>
                                                <button type="submit" class="btn btn-form">Submit</button>
                                            </div>
                                        </div>                                      
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

<?= $this->endSection(); ?>