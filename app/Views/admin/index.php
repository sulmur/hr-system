<?= $this->extend('layout/template_admin'); ?>

<?= $this->section('content'); ?>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->

<div class="content-body" style="min-height: 891px;">

    <div class="row page-titles mx-0"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="mb-5" style="margin-left:30px">
                            <h2 class="mt-2">Total Pegawai</h2>
                        </div>
                        <!-- <button onclick="Swal.fire()">Test</button> -->
                        <?= view('Myth\Auth\Views\_message_block') ?>

                        <div id="flotPie1" class="flot-chart m-5"></div>

                        <!-- <div id="morris-donut-chart"></div> -->

                        <!-- <canvas id="pieChart" width="250" height="250"></canvas> -->

                        <div id="accordion-three" class="accordion mx-4">
                            <div class="card shadow-sm">
                                <div class="card-header">
                                    <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4"><i class="fa" aria-hidden="true"></i> Jumlah Permohonan Cuti = <?= $total_cuti ?></h5>
                                </div>
                                <div id="collapseOne4" class="collapse" data-parent="#accordion-three">
                                    <div class="card-body shadow-sm">
                                        <div class="table-responsive">
                                            <table class="table table-hover zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor Karyawan</th>
                                                        <th>Nama Lengkap</th>
                                                        <th>Lama Cuti</th>
                                                        <th>Waktu Cuti</th>
                                                        <!-- <th>Aksi</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i = 1; ?>
                                                <?php foreach ($cuti->getResultArray() as $c) : ?>
                                                    <tr>
                                                        <td><?= $i++ ?></td>
                                                        <td><?= $c['nomor_karyawan']; ?></td>
                                                        <td><?= $c['nama_karyawan']; ?></td>
                                                        <td><?= $c['lama_cuti']; ?></td>
                                                        <td><?= $c['waktu_cuti']; ?></td>
                                                        <!-- <td><button type="button" style="border: 0px transparent"><i class="fa fa-print fa-2x" style="color:#4F8937"></i></button></td> -->
                                                    </tr>      
                                                    <?php endforeach; ?>                                       
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card shadow-sm">
                                <div class="card-header">
                                    <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo5" aria-expanded="false" aria-controls="collapseTwo5"><i class="fa" aria-hidden="true"></i> Jumlah Pengunduran Diri = <?= $total_pengunduran ?></h5>
                                </div>
                                <div id="collapseTwo5" class="collapse" data-parent="#accordion-three">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor Karyawan</th>
                                                        <th>Nama Lengkap</th>
                                                        <th>Alasan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1; ?>
                                                    <?php foreach ($pengunduran->getResultArray() as $p) : ?>
                                                    <tr>
                                                        <td><?= $i++ ?></td>
                                                        <td><?= $p['nomor_karyawan']; ?></td>
                                                            <td><?= $p['nama_karyawan']; ?></td>
                                                            <td><?= $p['alasan']; ?></td>
                                                    </tr>  
                                                    <?php endforeach; ?>                                           
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card shadow-sm">
                                <div class="card-header">
                                    <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree6" aria-expanded="false" aria-controls="collapseThree6"><i class="fa" aria-hidden="true"></i> Jumlah Pelatihan = <?= $total_pelatihan ?></h5>
                                </div>
                                <div id="collapseThree6" class="collapse" data-parent="#accordion-three">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-striped table-bordered zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Pelatihan</th>
                                                        <th>Tanggal</th>
                                                        <th>Materi Pelatihan</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i = 1; ?>
                                                <?php foreach ($pelatihan as $p) : ?>
                                                    <tr>
                                                        <td><?= $i++ ?></td>
                                                        <td><?= $p->nama_pelatihan; ?></td>
                                                        <td><?= $p->waktu_pelatihan; ?></td>
                                                        <td><?= $p->materi_pelatihan; ?></td>
                                                        <td>
                                                            <div class="badge badge-success badge-pill p-2"><h6 class="text-white mb-0 font-weight-semi-bold">Selesai</h6></div>
                                                        </td>
                                                    </tr>                                                              
                                                <?php endforeach; ?>                 
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->

<?= $this->endSection(); ?>