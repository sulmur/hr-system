<?= $this->extend('layout/template_admin'); ?>

<?= $this->section('content'); ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <ul class="nav nav-pills mb-4" id="pills-Tab" role="tablist">
                    <li class="nav-item mr-2" role="presentation">
                        <a class="btn btn-contact active" id="pills-absen-tab" data-toggle="pill" href="#pills-absen" role="tab" aria-controls="pills-absen" aria-selected="true">Absensi</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="btn btn-contact" id="pills-lembur-tab" data-toggle="pill" href="#pills-lembur" role="tab" aria-controls="pills-lembur" aria-selected="false">Lembur</a>
                    </li>
                </ul>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            
                            <div class="tab-content" id="pills-tabContent">                                
                                <div class="tab-pane fade show active" id="pills-absen" role="tabpanel" aria-labelledby="pills-absen-tab">
                                    <div class="card-body">
                                            <h4 class="card-title mt-2 float-left" style="margin-left:30px">Absensi Karyawan</h4>          
                                            <button class="btn btn-form float-right mr-4" onclick="window.print()"><i class="fa fa-print fa-lg text-white"> <span style="font-family:'Poppins'; font-size:16px"> Laporan</span></i></button>                          
                                        <div class="table-responsive">
                                            <table class="table  table-striped table-bordered zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Karyawan</th>
                                                        <th>Nomor Karyawan</th>
                                                        <th>Divisi</th>
                                                        <!-- <th>Waktu</th> -->
                                                        <th>Waktu & Tanggal</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i = 1; ?>
                                                <?php foreach ($absensi as $a) : ?>
                                                    <tr>
                                                        <td><?= $i++ ?></td>
                                                        <td><?= $a['nama_karyawan']; ?></td>
                                                        <td><?= $a['nomor_karyawan']; ?></td>
                                                        <td><?= $a['divisi']; ?></td>
                                                        <!-- <td><?= $a['waktu']; ?></td> -->
                                                        <td><?= $a['created_at']; ?></td>
                                                    </tr>  
                                                <?php endforeach; ?>                                          
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="pills-lembur" role="tabpanel" aria-labelledby="pills-lembur-tab">
                                    <div class="card-body">
                                            <h4 class="card-title mt-2 float-left" style="margin-left:30px">Lembur Karyawan</h4>  
                                            <button class="btn btn-form float-right mr-4" onclick="window.print()"><i class="fa fa-print fa-lg text-white"> <span style="font-family:'Poppins'; font-size:16px"> Laporan</span></i></button>                                  
                                        <div class="table-responsive">
                                            <table class="table table-hover table-striped table-bordered zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Karyawan</th>
                                                        <th>Nomor Karyawan</th>
                                                        <th>Divisi</th>
                                                        <th>Tanggal</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i = 1; ?>
                                                <?php foreach ($lembur as $l) : ?>
                                                    <tr>
                                                        <td><?= $i++ ?></td>
                                                        <td><?= $l['nama_karyawan']; ?></td>
                                                        <td><?= $l['nomor_karyawan']; ?></td>
                                                        <td><?= $l['divisi']; ?></td>
                                                        <td><?= $l['tanggal_lembur']; ?></td>
                                                    </tr>      
                                                <?php endforeach; ?>                                      
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

<?= $this->endSection(); ?>