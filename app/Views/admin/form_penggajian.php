<?= $this->extend('layout/template_admin'); ?>

<?= $this->section('content'); ?>

<div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mb-3">Penggajian</h4>
                                <?php if  (session()->get('pesan')) : ?>
                                    <div class="alert alert-success mb-3 alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                            <strong><?= session()->getFlashdata('pesan'); ?></strong>
                                    </div>
                                <?php endif; ?>
                                <div class="form-validation">
                                    <form class="form-valide" action="admin/penggajian/save" method="post">
                                    <?= csrf_field(); ?>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="nomor_karyawan">Nomor Karyawan <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('nomor_karyawan')) ? 'is-invalid' : ''; ?>" id="nomor_karyawan" name="nomor_karyawan" placeholder="Masukkan nomor karyawan" autofocus>
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('nomor_karyawan'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="nama_karyawan">Nama Lengkap <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('nama_karyawan')) ? 'is-invalid' : ''; ?>" id="nama_karyawan" name="nama_karyawan" placeholder="Masukkan nama lengkap">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('nama_karyawan'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="jabatan">Jabatan <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('jabatan')) ? 'is-invalid' : ''; ?>" id="jabatan" name="jabatan" placeholder="Masukkan Jabatan">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('jabatan'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="divisi">Divisi <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <select class="form-control <?= ($validation->hasError('divisi')) ? 'is-invalid' : ''; ?>" id="divisi" name="divisi" value="<?= old('divisi'); ?>">
                                                    <option value="">Pilih Divisi</option>
                                                    <option value="HR">HR</option>
                                                    <option value="Finance">Finance</option>
                                                    <option value="Sales & Marketing">Sales & Marketing</option>
                                                    <option value="Produksi">Produksi</option>
                                                    <option value="Purchasing">Purchasing</option>
                                                    <option value="Inventory">Inventory</option>
                                                </select>
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('divisi'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="total_gaji">Total Gaji <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control <?= ($validation->hasError('total_gaji')) ? 'is-invalid' : ''; ?>" id="total_gaji" name="total_gaji" placeholder="Masukkan Total gaji">
                                                <div class="invalid-feedback">
                                                    <?= $validation->getError('total_gaji'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-8 float-left">
                                                <a href="/data-pegawai" class="btn btn-outline-form">Batal</a>
                                                <button type="submit" class="btn btn-form">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

<?= $this->endSection(); ?>