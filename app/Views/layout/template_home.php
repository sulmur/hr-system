<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title><?= $title; ?></title>
    <!-- Favicon icon -->
    <link rel="icon" type="image" sizes="16x16" href="assets/images/logo-pertani.svg">
    <!-- Pignose Calender -->
    <link href="assets/plugins/pg-calendar/css/pignose.calendar.min.css" rel="stylesheet">
    <!-- Chartist -->
    <link rel="stylesheet" href="assets/plugins/chartist/css/chartist.min.css">
    <link rel="stylesheet" href="assets/plugins/chartist-plugin-tooltips/css/chartist-plugin-tooltip.css">
    <!-- Custom Stylesheet -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">

</head>
<body class="h-100" data-spy="scroll" data-target="#home-nav">

<nav id="home-nav" class="navbar fixed-top navbar-expand-lg navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="#">
            <img class="logo" src="assets/images/navbar/logo-pertani.svg" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link font-weight-bold poppins" data-scroll href="#join">Beranda</a>
            </li>
            <li class="nav-item">
                <a class="nav-link font-weight-bold poppins" data-scroll href="#alur">Alur Pendaftaran</a>
            </li>
            <li class="nav-item">
                <a class="nav-link font-weight-bold poppins" data-scroll href="#form">Formulir Pendaftaran</a>
            </li>
            </ul>
            <div class="my-2 my-lg-0">
                <a class="btn btn-contact font-weight-bold rounded-pill" data-scroll href="#contact">Kontak Kami</a>
                <a class="btn btn-outline-form font-weight-bold rounded-pill ml-2" data-scroll href="/login">Login</a>
            </div>
        </div>
    </div>
</nav>

<?= $this->renderSection('content'); ?>

<footer id="footer">
    <div class="container pt-5">
        <div class="row text-white">
            <div class="col-md-4">
                <h3 class="poppins font-weight-bold" style="height:40px; color: #fff">PT. PERTANI</h3>
                <p class="poppins mb-4">Mari bergabung menjadi bagian dari kami</p>
                <ul class="d-flex">
                    <li class="mr-4"><a href="#"><i class="fa fa-google fa-lg" style="color: #fff"></i></a></li>
                    <li class="mr-4"><a href="#"><i class="fa fa-facebook fa-lg" style="color: #fff"></i></a></li>
                    <li class="mr-4"><a href="#"><i class="fa fa-twitter fa-lg" style="color: #fff"></i></a></li>
                    <li class="mr-4"><a href="#"><i class="fa fa-instagram fa-lg" style="color: #fff"></i></a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <h4 class="poppins" style="height:40px; color: #fff">Sitemaps</h4>
                <ul>
                    <li><a class="text-site poppins" href="#">Beranda</a></li>
                    <li><a class="text-site poppins" href="#alur">Alur Pendaftaran</a></li>
                    <li><a class="text-site poppins" href="#form">Formulir Pendaftaran</a></li>
                    <li><a class="text-site poppins" href="#contact">Kontak Kami</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <h4 class="poppins" style="height:40px; color: #fff">Kontak Kami</h4>
                <ul>
                    <li><a class="text-site poppins" href="#">(021) 7993108</a></li>
                    <li><a class="text-site poppins" href="#alur">pusat@pertani.co.id</a></li>
                    <li><a class="text-site poppins" href="#form">Google Maps</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
    

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="assets/plugins/common/common.min.js"></script>
    <script src="assets/js/custom.min.js"></script>
    <script src="assets/js/settings.js"></script>
    <script src="assets/js/gleek.js"></script>
    <script src="assets/js/styleSwitcher.js"></script>

    <!-- Chartjs -->
    <script src="assets/plugins/chart.js/Chart.bundle.min.js"></script>
    <!-- Circle progress -->
    <script src="assets/plugins/circle-progress/circle-progress.min.js"></script>
    <!-- Datamap -->
    <script src="assets/plugins/d3v3/index.js"></script>
    <script src="assets/plugins/topojson/topojson.min.js"></script>
    <script src="assets/plugins/datamaps/datamaps.world.min.js"></script>
    <!-- Morrisjs -->
    <script src="assets/plugins/raphael/raphael.min.js"></script>
    <script src="assets/plugins/morris/morris.min.js"></script>
    <!-- Pignose Calender -->
    <script src="assets/plugins/moment/moment.min.js"></script>
    <script src="assets/plugins/pg-calendar/js/pignose.calendar.min.js"></script>
    <!-- ChartistJS -->
    <script src="assets/plugins/chartist/js/chartist.min.js"></script>
    <script src="assets/plugins/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js"></script>

    <script src="assets/js/dashboard/dashboard-1.js"></script>

    <script src="assets/sweetalert/sweetalert2.all.js"></script>

    <script src="assets/js/script.js"></script>

    <script>
        function previewFile() {
            const cv = document.querySelector('#cv');
            const cvLabel = document.querySelector('.custom-file-label');
            const imgPreview = document.querySelector('.img-preview');

            cvLabel.textContent = cv.files[0].name;

            const fileCv = new FileReader();
            fileCv.readAsDataURL(cv.files[0]);

            fileCv.onload = function(e) {
                imgPreview.src = e.target.result;
            }
        }
        

    </script>

</body>
</html>