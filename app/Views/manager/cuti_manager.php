<?= $this->extend('layout/template_manager'); ?>

<?= $this->section('content'); ?>

<style>
    @media print {
        th:nth-child(6),
        td:nth-child(6)
        {
            display: none;
        }
    }
</style>

<div class="swal2" data-swal2="<?= session()->get('pesan'); ?>"></div>
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                    <h4 class="card-title mt-2" style="margin-left:30px">Permohonan Cuti Karyawan</h4>
                                    <button class="btn btn-form float-right mr-4" onclick="window.print()"><i class="fa fa-print fa-lg text-white"> <span style="font-family:'Poppins'; font-size:16px"> Laporan</span></i></button>
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>ID Cuti</th>
                                                <th>Nomor Karyawan</th>
                                                <th>Nama Lengkap</th>
                                                <th>Lama Cuti</th>
                                                <th>Waktu Cuti</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($cuti->getResultArray() as $c) : ?>
                                            <tr>
                                                <td>CT00<?= $c['id_cuti']; ?></td>
                                                <td><?= $c['nomor_karyawan']; ?></td>
                                                <td><?= $c['nama_karyawan']; ?></td>
                                                <td><?= $c['lama_cuti']; ?></td>
                                                <td><?= $c['waktu_cuti']; ?></td>
                                                <td>                                                
                                                    <button type="button" class="btn btn-sm btn-form"><i class="fa fa-check-circle fa-lg"></i></button>
                                                    <!-- <button type="button" class="border-0 bg-transparent" data-toggle="modal" data-target="#hapus-cuti"><i class="fa fa-times-circle fa-2x ml-2" style="color:red"></i></button> -->
                                                    <form action="/manager/cuti/hapus/<?= $c['id_cuti']; ?>" style="display:contents" method="post">
                                                        <?= csrf_field(); ?>
                                                        <input type="hidden" name="_method" value="DELETE">    
                                                        <button type="submit" class="btn btn-sm btn-danger" id="<?= $c['id_cuti']; ?>"><i class="fa fa-times-circle fa-lg"></i></button>
                                                    </form> 
                                                </td>
                                            </tr>  

                                            <!-- <div class="modal fade" id="hapus-cuti">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header text-center">
                                                            <h5 class="modal-title">Tolak Cuti</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4>Apakah anda yakin menolak cuti ini?</h4>
                                                        </div>
                                                        <div class="modal-footer text-center">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>

                                                            <form action="/manager/cuti/hapus/<?= $c['id_cuti']; ?>" method="post">
                                                                <?= csrf_field(); ?>
                                                                <input type="hidden" name="_method" value="DELETE">    
                                                                <button type="submit" class="btn btn-success text-white" id="<?= $c['id_cuti']; ?>">Ya</button>
                                                            </form> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>               -->

                                        <?php endforeach; ?>                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

<?= $this->endSection(); ?>