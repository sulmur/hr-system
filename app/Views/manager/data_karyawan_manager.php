<?= $this->extend('layout/template_manager'); ?>

<?= $this->section('content'); ?>

<style>
    @media print {
        th:nth-child(5),
        td:nth-child(5)
        {
            display: none;
        }
    }
</style>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mt-2 float-left" style="margin-left:30px">Data Karyawan</h4>
                                <button class="btn btn-form float-right mr-4" onclick="window.print()"><i class="fa fa-print fa-lg text-white"> <span style="font-family:'Poppins'; font-size:16px"> Laporan</span></i></button>
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Karyawan</th>
                                                <th>Nomor Karyawan</th>
                                                <th>Divisi</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($karyawan->getResultArray() as $k) : ?>
                                            <tr>
                                                <td><?= $i++ ?></td>
                                                <td><?= $k['nama_karyawan']; ?></td>
                                                <td><?= $k['nomor_karyawan']; ?></td>
                                                <td><?= $k['divisi']; ?></td>
                                                <td><button type="button" class="btn btn-sm btn-form text-white" data-toggle="modal" data-target="#detail-data-karyawan" id="btn-info" data-id="<?= $k['nomor_karyawan']; ?>" data-nomor="<?= $k['nomor_karyawan']; ?>" data-nama="<?= $k['nama_karyawan']; ?>" data-divisi="<?= $k['divisi']; ?>" data-jabatan="<?= $k['jabatan']; ?>" data-tanggal-masuk="<?= $k['tanggal_masuk']; ?>" data-tanggal-lahir="<?= $k['tanggal_lahir']; ?>" data-agama="<?= $k['agama']; ?>" data-alamat="<?= $k['alamat']; ?>"><i class="fa fa-info-circle fa-lg"></i></button></td>
                                            </tr>  

                                            <!-- Modal Detail -->
                                            <div class="modal fade" id="detail-data-karyawan">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Detail Karyawan</h5>
                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Nomor Pegawai</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p id="nomor_karyawan">: <?= $k['nomor_karyawan']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Nama</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $k['nama_karyawan']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Divisi</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $k['divisi']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Jabatan</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $k['jabatan']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Tanggal Masuk</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $k['tanggal_masuk']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Tanggal Lahir</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $k['tanggal_lahir']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Agama</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $k['agama']; ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <b>Alamat</b>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p>: <?= $k['alamat']; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 

                                        <?php endforeach; ?>                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

<?= $this->endSection(); ?>