<?= $this->extend('layout/template_manager'); ?>

<?= $this->section('content'); ?>

<style>
    @media print {
        th:nth-child(3),
        td:nth-child(3)
        {
            display: none;
        }
    }
</style>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body"> 
                                <h4 class="card-title mt-2 float-left" style="margin-left:30px">Penggajian Karyawan</h4>
                                <button class="btn btn-form float-right mr-4" onclick="window.print()"><i class="fa fa-print fa-lg text-white"> <span style="font-family:'Poppins'; font-size:16px"> Laporan</span></i></button>
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Karyawan</th>
                                                <th>Nomor Karyawan</th>
                                                <th>Jabatan</th>
                                                <th>Divisi</th>
                                                <th>Total Gaji</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($penggajian as $pe) : ?>
                                            <tr>
                                                <td><?= $i++ ?></td>
                                                <td><?= $pe['nama_karyawan']; ?></td>
                                                <td><?= $pe['nomor_karyawan']; ?></td>
                                                <td><?= $pe['jabatan']; ?></td>
                                                <td><?= $pe['divisi']; ?></td>
                                                <td><?= $pe['total_gaji']; ?></td>
                                                <td><div class="badge badge-danger badge-pill"><h6 class=" text-white mt-1 mb-2 mx-1 font-weight-semi-bold">Belum Dibayar</h6></div></td>\
                                            </tr>   
                                        <?php endforeach; ?>                                         
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

<?= $this->endSection(); ?>