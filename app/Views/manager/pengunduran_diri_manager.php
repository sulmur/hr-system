<?= $this->extend('layout/template_manager'); ?>

<?= $this->section('content'); ?>

<style>
    @media print {
        th:nth-child(5),
        td:nth-child(5)
        {
            display: none;
        }
    }
</style>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mt-2 float-left" style="margin-left:30px">Pengunduran Diri Karyawan</h4>
                                <button class="btn btn-form float-right mr-4" onclick="window.print()"><i class="fa fa-print fa-lg text-white"> <span style="font-family:'Poppins'; font-size:16px"> Laporan</span></i></button>
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>ID Pengunduran Diri</th>
                                                <th>Nomor Karyawan</th>
                                                <th>Nama Lengkap</th>
                                                <th>Alasan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($pengunduran->getResultArray() as $p) : ?>
                                            <tr>
                                                <td>PD00<?= $p['id_pengunduran_diri']; ?></td>
                                                <td><?= $p['nomor_karyawan']; ?></td>
                                                <td><?= $p['nama_karyawan']; ?></td>
                                                <td><?= $p['alasan']; ?></td>
                                                <td>
                                                <button type="button" class="btn btn-sm btn-form"><i class="fa fa-check-circle fa-lg"></i></button>
                                                    <!-- <button type="button" class="border-0 bg-transparent" data-toggle="modal" data-target="#hapus-cuti"><i class="fa fa-times-circle fa-2x ml-2" style="color:red"></i></button> -->
                                                    <form action="/manager/pengundurandiri/hapus/<?= $p['id_pengunduran_diri']; ?>" style="display:contents" method="post">
                                                        <?= csrf_field(); ?>
                                                        <input type="hidden" name="_method" value="DELETE">    
                                                        <button type="submit" class="btn btn-sm btn-danger" id="<?= $p['id_pengunduran_diri']; ?>"><i class="fa fa-times-circle fa-lg"></i></button>
                                                    </form> 
                                                </td>
                                            </tr>  

                                            <div class="modal fade" id="hapus-pengunduran-karyawan">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header text-center">
                                                            <h5 class="modal-title">Tolak Pengunduran Diri</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4>Apakah anda yakin menolak pengunduran diri <?= $p['nama_karyawan'];?>?</h4>
                                                        </div>
                                                        <div class="modal-footer text-center">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>

                                                            <form action="/manager/pengundurandiri/hapus/<?= $p['id_pengunduran_diri']; ?>" method="post">
                                                                <?= csrf_field(); ?>
                                                                <input type="hidden" name="_method" value="DELETE">    
                                                                <button type="submit" class="btn btn-success text-white" id="<?= $p['id_pengunduran_diri']; ?>">Ya</button>
                                                            </form> 

                                                            <!-- <a href="/admin/datakaryawan/delete/" class="btn btn-success text-white">Ya</a> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>            
                                        <?php endforeach; ?>                                         
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

<?= $this->endSection(); ?>