<?= $this->extend('layout/template_manager'); ?>

<?= $this->section('content'); ?>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="loader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
        </svg>
    </div>
</div>
<!--*******************
    Preloader end
********************-->

<div class="content-body" style="min-height: 891px;">

    <div class="row page-titles mx-0">
        <!-- <div class="col p-md-0">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
            </ol>
        </div> -->
    </div>
    <!-- row -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="mb-5" style="margin-left:30px">
                            <h2 class="mt-2">Total Pegawai</h2>
                        </div>

                        <div id="flotPie1" class="flot-chart m-5"></div>

                        <!-- <div class="row mx-3">
                            <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-1">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">HRD</h3>
                                        <div class="d-inline-block">
                                            <h4 class="text-white">#</h4>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-users"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-2">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">Finance</h3>
                                        <div class="d-inline-block">
                                            <h4 class="text-white">#</h4>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-money"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-3">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">Production</h3>
                                        <div class="d-inline-block">
                                            <h4 class="text-white">#</h4>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-industry"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mx-3">
                            <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-5">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">Purchasing</h3>
                                        <div class="d-inline-block">
                                            <h4 class="text-white">#</h4>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-credit-card"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-7">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">Sales & Marketing</h3>
                                        <div class="d-inline-block">
                                            <h4 class="text-white">#</h4>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-shopping-cart"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-6">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">Inventory</h3>
                                        <div class="d-inline-block">
                                            <h4 class="text-white">#</h4>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-truck text-white"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div> -->


                        <!-- <div class="col-lg-4 col-sm-6">
                                <div class="card gradient-4">
                                    <div class="card-body">
                                        <h3 class="card-title text-white">Customer Satisfaction</h3>
                                        <div class="d-inline-block">
                                            <h2 class="text-white">99%</h2>
                                            
                                        </div>
                                        <span class="float-right display-5 opacity-5"><i class="fa fa-heart"></i></span>
                                    </div>
                                </div>
                            </div> -->

                        <div id="accordion-three" class="accordion mx-4">
                            <div class="card shadow-sm">
                                <div class="card-header">
                                    <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4"><i class="fa" aria-hidden="true"></i> Jumlah Permohonan Cuti = <?= $total_cuti ?></h5>
                                </div>
                                <div id="collapseOne4" class="collapse" data-parent="#accordion-three">
                                    <div class="card-body shadow-sm">
                                        <div class="table-responsive">
                                            <table class="table table-hover zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor Karyawan</th>
                                                        <th>Nama Lengkap</th>
                                                        <th>Lama Cuti</th>
                                                        <th>Waktu Cuti</th>
                                                        <!-- <th>Aksi</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1; ?>
                                                    <?php foreach ($cuti->getResultArray() as $c) : ?>
                                                        <tr>
                                                            <td><?= $i++ ?></td>
                                                            <td><?= $c['nomor_karyawan']; ?></td>
                                                            <td><?= $c['nama_karyawan']; ?></td>
                                                            <td><?= $c['lama_cuti']; ?></td>
                                                            <td><?= $c['waktu_cuti']; ?></td>
                                                            <!-- <td><button type="button" style="border: 0px transparent"><i class="fa fa-print fa-2x" style="color:#4F8937"></i></button></td> -->
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card shadow-sm">
                                <div class="card-header">
                                    <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo5" aria-expanded="false" aria-controls="collapseTwo5"><i class="fa" aria-hidden="true"></i> Jumlah Pengunduran Diri = <?= $total_pengunduran ?></h5>
                                </div>
                                <div id="collapseTwo5" class="collapse" data-parent="#accordion-three">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor Karyawan</th>
                                                        <th>Nama Lengkap</th>
                                                        <th>Alasan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1; ?>
                                                    <?php foreach ($pengunduran->getResultArray() as $p) : ?>
                                                        <tr>
                                                            <td><?= $i++ ?></td>
                                                            <td><?= $p['nomor_karyawan']; ?></td>
                                                            <td><?= $p['nama_karyawan']; ?></td>
                                                            <td><?= $p['alasan']; ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card shadow-sm">
                                <div class="card-header">
                                    <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree6" aria-expanded="false" aria-controls="collapseThree6"><i class="fa" aria-hidden="true"></i> Jumlah Pelatihan = <?= $total_pelatihan ?></h5>
                                </div>
                                <div id="collapseThree6" class="collapse" data-parent="#accordion-three">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-striped table-bordered zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Pelatihan</th>
                                                        <th>Tanggal</th>
                                                        <th>Materi Pelatihan</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1; ?>
                                                    <?php foreach ($pelatihan as $p) : ?>
                                                        <tr>
                                                            <td><?= $i++ ?></td>
                                                            <td><?= $p->nama_pelatihan; ?></td>
                                                            <td><?= $p->waktu_pelatihan; ?></td>
                                                            <td><?= $p->materi_pelatihan; ?></td>
                                                            <td>
                                                                <div class="badge badge-success badge-pill p-2">
                                                                    <h6 class="text-white mb-0 font-weight-semi-bold">Selesai</h6>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->

<?= $this->endSection(); ?>