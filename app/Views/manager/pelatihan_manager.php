<?= $this->extend('layout/template_manager'); ?>

<?= $this->section('content'); ?>

<style>
    @media print {
        @page {}

        th:nth-child(5),
        td:nth-child(5),
        #debug-icon-link {
            display: none;
        }

        .float-left {
            text-align: center !important;
        }
    }
</style>

<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">

    <div class="row page-titles mx-0">
        <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
    </div>
    <!-- row -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="mb-5">
                            <h4 class="card-title mt-2 float-left" style="margin-left:30px">Pelatihan Karyawan</h4>
                            <button class="btn btn-form float-right mr-4" onclick="window.print()"><i class="fa fa-print fa-lg text-white"> <span style="font-family:'Poppins'; font-size:16px"> Laporan</span></i></button>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered zero-configuration">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Pelatihan</th>
                                        <th>Tanggal</th>
                                        <th>Materi Pelatihan</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($pelatihan as $p) : ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <td><?= $p->nama_pelatihan; ?></td>
                                            <td><?= $p->waktu_pelatihan; ?></td>
                                            <td><?= $p->materi_pelatihan; ?></td>
                                            <td>
                                                <div class="badge badge-success badge-pill p-2">
                                                    <h6 class="text-white mb-0 font-weight-semi-bold">Selesai</h6>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
            Content body end
        ***********************************-->

<?= $this->endSection(); ?>