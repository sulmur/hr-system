<?= $this->extend('layout/template_manager'); ?>

<?= $this->section('content'); ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body"> 
                                <h4 class="card-title mt-2 float-left" style="margin-left:30px">Laporan</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>Cetak Laporan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Cetak Laporan Data Karyawan</td>
                                                <td><i class="fa fa-print fa-2x" style="color:#4F8937"></i></td>
                                            </tr>           
                                            <tr>
                                                <td>Cetak Laporan Absensi</td>
                                                <td><i class="fa fa-print fa-2x" style="color:#4F8937"></i></td>
                                            </tr>
                                            <tr>
                                                <td>Cetak Laporan Cuti</td>
                                                <td><i class="fa fa-print fa-2x" style="color:#4F8937"></i></td>
                                            </tr>  
                                            <tr>
                                                <td>Cetak Laporan Pengunduran Diri</td>
                                                <td><i class="fa fa-print fa-2x" style="color:#4F8937"></i></td>
                                            </tr>              
                                            <tr>
                                                <td>Cetak Laporan Penggajian</td>
                                                <td><i class="fa fa-print fa-2x" style="color:#4F8937"></i></td>
                                            </tr>                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

<?= $this->endSection(); ?>