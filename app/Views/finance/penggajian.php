<?= $this->extend('layout/template_finance'); ?>

<?= $this->section('content'); ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <!-- <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div> -->
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body"> 
                                <h4 class="card-title mt-2 float-left" style="margin-left:30px">Penggajian Karyawan</h4>
                                <button class="btn btn-form float-right mr-4" onclick="window.print()"><i class="fa fa-print fa-lg text-white"> <span style="font-family:'Poppins'; font-size:16px"> Laporan</span></i></button>
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Karyawan</th>
                                                <th>Nomor Karyawan</th>
                                                <th>Jabatan</th>
                                                <th>Divisi</th>
                                                <th>Total Gaji</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($penggajian as $p) : ?>
                                            <tr>
                                                <td><?= $i++ ?></td>
                                                <td><?= $p['nama_karyawan']; ?></td>
                                                <td><?= $p['nomor_karyawan']; ?></td>
                                                <td><?= $p['jabatan']; ?></td>
                                                <td><?= $p['divisi']; ?></td>
                                                <td><?= $p['total_gaji']; ?></td>
                                                <td><div class="badge badge-danger badge-pills text-white"><h6 class=" text-white mt-1 mb-2 mx-1">Belum Dibayar</h6></div></td>
                                            </tr>     
                                        <?php endforeach; ?>                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

<?= $this->endSection(); ?>